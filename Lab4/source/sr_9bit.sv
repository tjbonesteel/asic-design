// $Id: $
// File name:   sr_9bit.sv
// Created:     2/11/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: 9-bit shift register

module sr_9bit
  (
   input wire clk,
   input wire n_rst,
   input wire shift_strobe,
   input wire serial_in,
   output wire [7:0]packet_data,
   output wire stop_bit
   );

   reg [8:0]   data;


   flex_stp_sr #(9,0) shift_9bit(.clk(clk), .n_rst(n_rst), .shift_enable(shift_strobe), .serial_in(serial_in), .parallel_out(data));

   assign packet_data[7:0] = data[7:0];

   assign stop_bit = data[8];

endmodule // sr_9bit
