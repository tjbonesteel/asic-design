// $Id: $
// File name:   mealy.sv
// Created:     2/10/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Mealy 1101 detector
 module mealy
(
   input wire clk,
   input wire n_rst,
   input wire i,
   output reg o
   );

   typedef enum {st_0, st_1, st_2, st_3, st_4} state;
   state c_state, n_state;

   always_ff @( posedge clk, negedge n_rst)
     begin

	if(n_rst == 1'b0)
	  c_state <= st_0;
	else
	  c_state <= n_state;
     end

   always@(c_state, i)
     begin
	n_state = c_state;
	o = 0;
   
	case(c_state)
	  st_0: begin
	     if(i == 1)
	       n_state = st_1;
	     else
	       n_state = st_0;
	  end
	  st_1: begin
	     if(i == 1)
	       n_state = st_2;
	     else  
	       n_state = st_0;
	  end
	  st_2: begin
	     if(i == 0)
	       n_state = st_3;
	     else 
	       n_state = st_2;
	  end
	  st_3: begin
	    if(i == 1)
	      n_state = st_4;
	    else 
	      n_state = st_0;
	  end
	  st_4: begin
	     if(i == 1)begin
		n_state = st_1;
		o = 1;
	     end
	     else 
	       n_state = st_0;
	  end
	  default: n_state = st_0;
	endcase // case (c_state)
     end // always_comb@ (c_state, i)

   

endmodule // moore
