// $Id: $
// File name:   tb_mealy.sv
// Created:     2/10/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Test bench for mealy 1101 detector



`timescale 1ns/100ps

module tb_mealy
  ();

   localparam CLK_PERIOD = 2.5;
   reg tb_clk;
   reg tb_nrst;
   reg tb_i;
   reg tb_o;
   
   moore DUT(.clk(tb_clk), .n_rst(tb_nrst), .i(tb_i), .o(tb_o));

   //CLK gen
   always begin
      tb_clk = 1'b0;
      #(CLK_PERIOD/2);
      tb_clk = 1'b1;
      #(CLK_PERIOD/2);
   end
   
   
   initial begin
      tb_nrst = 1'b0;
      #5;
      tb_nrst = 1'b1;
      @(posedge tb_clk);

      tb_i = 1'b1;
      @(posedge tb_clk);
      
      assert(tb_o == 1'b0) begin
	$display("Test 1 - PASS");
      end else begin
	 $error("Test 1 - FAIL!"); 
      end // UNMATCHED !!
      
      tb_i = 1'b1;

      
      @(posedge tb_clk);
      
      assert(tb_o == 1'b0) begin
	$display("Test 1 - PASS");
      end else begin
	 $error("Test 1 - FAIL!"); 
      end // UNMATCHED !!
      
      
      tb_i = 1'b0; #2;
      
      @(posedge tb_clk);
      
      assert(tb_o == 1'b0) begin
	$display("Test 1 - PASS");
      end else begin
	 $error("Test 1 - FAIL!"); 
      end // UNMATCHED !!
      
      
      tb_i = 1'b1;
      
      @(posedge tb_clk);
      
      assert(tb_o == 1'b1) begin
	$display("Test 1 - PASS");
      end else begin
	 $error("Test 1 - FAIL!"); 
      end // UNMATCHED !!

      tb_nrst = 1'b0;
      #5;
      tb_nrst = 1'b1;
      #1;

      tb_i = 1'b1;
      #1;
      tb_i = 1'b1;
      #1;
      tb_i = 1'b1;
      #1;
      tb_i = 1'b1;
      #1;
      tb_i = 1'b0;
      #1;
      tb_i = 1'b1;
      #1;

      
      assert(tb_o == 1'b1) begin
	$display("Test 2 - PASS");
      end else begin
	 $error("Test 2 - FAIL!"); 
      end // UNMATCHED !!

      tb_nrst = 1'b0;
      #5;
      tb_nrst = 1'b1;
      #1;

      tb_i = 1'b1;
      #1;
      tb_i = 1'b1;
      #1;
      tb_i = 1'b1;
      #1;
      tb_i = 1'b1;
      #1;
      
      assert(tb_o == 1'b0) begin
	$display("Test 3 - PASS");
      end else begin
	 $error("Test 3 - FAIL!"); 
      end // UNMATCHED !!


      tb_i = 1'b1;
      #1;
      tb_i = 1'b0;
      #1;
      tb_i = 1'b1;
      #1;
      tb_i = 1'b0;
      #1;
      tb_i = 1'b1;
      #1;
      tb_i = 1'b1;
      #1;
      tb_i = 1'b0;
      #1;
      tb_i = 1'b1;

      assert(tb_o == 1'b1) begin
	$display("Test 4 - PASS");
      end else begin
	 $error("Test 4 - FAIL!"); 
      end // UNMATCHED !!
      

      
      
   end // initial begin
   
endmodule // tb_mealy
