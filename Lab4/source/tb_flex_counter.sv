// $Id: $
// File name:   tb_flex_counter.sv
// Created:     2/4/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: Test bench for flex counter
`timescale 1ns / 100ps
module tb_flex_counter ();
   //Parameters for test bench
   localparam CLK_PERIOD = 2.5;
   localparam CHECK_DELAY = 1;
   localparam NUM_CNT_BITS = 4;

   //test bench equivalent signals
   reg tb_clk;
   reg tb_n_rst;
   reg tb_clear;
   reg tb_count_enable;
   reg [(NUM_CNT_BITS-1):0] tb_rollover_val;
   reg [(NUM_CNT_BITS-1):0] tb_count_out;
   reg 			    tb_rollover_flag;
   integer 		    i;
   
   
   flex_counter DUT (.clk(tb_clk), .n_rst(tb_n_rst), .clear(tb_clear), 
		     .count_enable(tb_count_enable), .rollover_val(tb_rollover_val), 
		     .count_out(tb_count_out), .rollover_flag(tb_rollover_flag));

   //Standard clock generation statements
   always begin
      tb_clk = 1'b0;
      #(CLK_PERIOD/2.0);
      tb_clk = 1'b1;
      #(CLK_PERIOD/2.0);
      
   end

   
   initial 
     begin
      @(posedge tb_clk);
      tb_clear = 1'b1;
      @(posedge tb_clk);
      tb_clear = 1'b0;

      ////////TEST 1/////////
      tb_rollover_val = 7;
      tb_n_rst = 1'b1;
      tb_count_enable = 1'b0;
      tb_clear = 1'b1;

      @(posedge tb_clk);
      #(CHECK_DELAY);
      assert ((tb_count_out == 0) && (tb_rollover_flag == 0)) begin
	$info("Test case 1: Pass");
      end else begin
	 $error("Test case 1: FAILED");
      end // UNMATCHED !!

      @(posedge tb_clk);
      tb_clear = 1'b1;
      @(posedge tb_clk);
      tb_clear = 1'b0;

      ////////TEST 2/////////
      tb_rollover_val = 7;
      tb_n_rst = 1'b1;
      tb_count_enable = 1'b1;
      tb_clear = 1'b0;
	for(i = 0; i < 7; i = i + 1)
	  @(posedge tb_clk);
      #(CHECK_DELAY);
      assert ((tb_count_out == tb_rollover_val) && (tb_rollover_flag == 1)) begin
	$info("Test case 2: Pass");
      end else begin
	 $error("Test case 2: FAILED");
      end // UNMATCHED !!

      
      @(posedge tb_clk);
      tb_clear = 1'b1;
      @(posedge tb_clk);
      tb_clear = 1'b0;

      ////////TEST 3/////////
      tb_rollover_val = 5;
      tb_n_rst = 1'b0;
      tb_count_enable = 1'b1;
      tb_clear = 1'b0;

      @(posedge tb_clk);
      #(CHECK_DELAY);
      assert ((tb_count_out == 0) && (tb_rollover_flag == 0)) begin
	$info("Test case 3: Pass");
      end else begin
	 $error("Test case 3: FAILED");
      end 
      
      ////////TEST 4/////////
      @(posedge tb_clk);
      tb_clear = 1'b1;
      @(posedge tb_clk);
      tb_clear = 1'b0;
	
      tb_rollover_val = 7;
      tb_count_enable = 1'b0;
      tb_n_rst = 1'b1;
      tb_clear = 1'b0;

	for(i = 0; i < 3; i = i + 1)
	  @(posedge tb_clk);
      
      
      #(CHECK_DELAY);
      assert ((tb_count_out == 0) && (tb_rollover_flag == 0)) begin
	$info("Test case 4: Pass");
      end else begin
	 $error("Test case 4: FAILED");
      end

      
     
      
      ////////TEST 5/////////
      @(posedge tb_clk);
      tb_clear = 1'b1;
      @(posedge tb_clk);
      tb_clear = 1'b0;
      
      
      tb_rollover_val = 5;
      tb_count_enable = 1'b1;
      tb_n_rst = 1'b1;
      tb_clear = 1'b0;

	for(i = 0; i < 8; i = i + 1)
	  @(posedge tb_clk);

      #(CHECK_DELAY);
      assert ((tb_count_out == 3) && (tb_rollover_flag == 0)) begin
	$info("Test case 5: Pass");
      end else begin
	 $error("Test case 5: FAILED");
      end // UNMATCHED !!

      
      ////////TEST 6/////////
      @(posedge tb_clk);
      tb_clear = 1'b1;
      @(posedge tb_clk);
      tb_clear = 1'b0;
      
      
      tb_rollover_val = 6;
      tb_count_enable = 1'b1;
      tb_n_rst = 1'b1;
      tb_clear = 1'b0;

	for(i = 0; i < 6; i = i + 1)
	  @(posedge tb_clk);
      

      #(CHECK_DELAY);
      assert ((tb_count_out == tb_rollover_val) && (tb_rollover_flag == 1)) begin
	$info("Test case 6: Pass");
      end else begin
	 $error("Test case 6: FAILED");
      end

      
      ////////TEST 7/////////
      @(posedge tb_clk);
      tb_clear = 1'b1;
      @(posedge tb_clk);
      tb_clear = 1'b0;
      
      
      tb_rollover_val = 8;
      tb_count_enable = 1'b1;
      tb_n_rst = 1'b1;
      tb_clear = 1'b0;

	for(i = 0; i < 8; i = i + 1)
	  @(posedge tb_clk);

      #(CHECK_DELAY);
      assert ((tb_count_out == tb_rollover_val) && (tb_rollover_flag == 1)) begin
	$info("Test case 7: Pass");
      end else begin
	 $error("Test case 7: FAILED");
      end
      
      
      ////////TEST89/////////
      @(posedge tb_clk);
      tb_clear = 1'b1;
      @(posedge tb_clk);
      tb_clear = 1'b0;
      
      
      tb_rollover_val = 14;
      tb_count_enable = 1'b1;
      tb_n_rst = 1'b1;
      tb_clear = 1'b0;

	for(i = 0; i < 14; i = i + 1)
	  @(posedge tb_clk);

      #(CHECK_DELAY);
      assert ((tb_count_out == tb_rollover_val) && (tb_rollover_flag == 1)) begin
	$info("Test case 8: Pass");
      end else begin
	 $error("Test case 8: FAILED");
      end

      
      ////////TEST 9/////////
      @(posedge tb_clk);
      tb_clear = 1'b1;
      @(posedge tb_clk);
      tb_clear = 1'b0;
      
      
      tb_rollover_val = 6;
      tb_count_enable = 1'b1;
      tb_n_rst = 1'b1;
      tb_clear = 1'b0;

	for(i = 0; i < 7; i = i + 1)
	  @(posedge tb_clk);
     

      #(CHECK_DELAY);
      assert ((tb_count_out == 1) && (tb_rollover_flag == 0)) begin
	$info("Test case 9: Pass");
      end else begin
	 $error("Test case 9: FAILED");
      end

 ////////TEST 10/////////
      @(posedge tb_clk);
      tb_clear = 1'b1;
      @(posedge tb_clk);
      tb_clear = 1'b0;
      
      
      tb_rollover_val = 5;
      tb_count_enable = 1'b1;
      tb_n_rst = 1'b1;
      tb_clear = 1'b0;

      
      for(i = 0; i < 3; i = i + 1)
	@(posedge tb_clk);
      
      tb_n_rst = 1'b0;
      
      
      @(posedge tb_clk);
      #(CHECK_DELAY);
      assert ((tb_count_out == 0) && (tb_rollover_flag == 0)) begin
	$info("Test case 10: Pass");
      end else begin
	 $error("Test case 10: FAILED");
      end


	    
    end




endmodule
