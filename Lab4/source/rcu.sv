// $Id: $
// File name:   rcu.sv
// Created:     2/11/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Receiver Control Unit

module rcu
  (
   input wire clk,
   input wire n_rst,
   input wire start_bit_detected,
   input wire packet_done,
   input wire framing_error,
   output reg sbc_clear,
   output reg sbc_enable,
   output reg load_buffer,
   output reg enable_timer
   );

   typedef enum logic [2:0] { IDLE, READ, DONE, CHECK, STORE} state_type;
   state_type c_state, n_state;


   always_ff @(posedge clk, negedge n_rst) begin

      if(n_rst == 1'b0)
	 c_state <= IDLE;
      else
	c_state <= n_state;
   end

   always @ (c_state, start_bit_detected, packet_done, framing_error) begin
      n_state = c_state;
      
      case(c_state)
	IDLE: begin
	   if(start_bit_detected == 1'b0)
	     n_state = IDLE;
	   else
	     n_state = READ;
	end

	READ: begin
	   if(packet_done == 1'b1)
	     n_state = DONE;
	   else
	     n_state = READ;
	end

	DONE: begin
	   n_state = CHECK;
	end

	CHECK: begin
	   if(framing_error == 1'b0)
	     n_state = STORE;
	   else n_state = IDLE;
	end

	STORE: begin
	   n_state = IDLE;
	end
      endcase // case (c_state)
   end // always @ (c_state, start_bit_detected, packet_done, framing_error)


   always @ (n_state, n_rst) begin
      case(n_state)
	IDLE: begin
	   sbc_clear = 1'b0;
	   sbc_enable = 1'b0;
	   load_buffer = 1'b0;
	   enable_timer = 1'b0;
	end
	
	READ: begin
	   sbc_clear = 1'b1;
	   sbc_enable = 1'b0;
	   load_buffer = 1'b0;
	   enable_timer = 1'b1;
	end
	
	DONE: begin
	   sbc_clear = 1'b0;
	   sbc_enable = 1'b1;
	   load_buffer = 1'b0;
	   enable_timer = 1'b0;
	end
	CHECK: begin
	   sbc_clear = 1'b0;
	   sbc_enable = 1'b0;
	   load_buffer = 1'b0;
	   enable_timer = 1'b0;
	end
	STORE: begin
	   sbc_clear = 1'b0;
	   sbc_enable = 1'b0;
	   load_buffer = 1'b1;
	   enable_timer = 1'b0;
	end
	default: begin
	   sbc_clear = 1'b0;
	   sbc_enable = 1'b0;
	   load_buffer = 1'b0;
	   enable_timer = 1'b0;
	end
	
      endcase // case (n_state)
      
   end
   
endmodule // rcu
