// $Id: $
// File name:   timer.sv
// Created:     2/10/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Timer control block

module timer
  (
   input wire clk,
   input wire n_rst,
   input wire enable_timer,
   output wire shift_strobe,
   output wire packet_done
   );

   reg [3:0]   count_1;
   reg [3:0]   count_2;
   reg 	       flip_flop;
   reg 	       enable;

   flex_counter count1 ( .clk(clk), .n_rst(n_rst), .clear(packet_done), .count_enable(enable), .rollover_val(4'b1010), .count_out(count_1), .rollover_flag(shift_strobe));
   flex_counter count2 ( .clk(clk), .n_rst(n_rst), .clear(packet_done), .count_enable(shift_strobe), .rollover_val(4'b1001), .count_out(count_2), .rollover_flag(packet_done));
   



   always_ff @ (posedge clk, negedge n_rst) begin
      if(n_rst == 1'b0) begin
	 enable <= 1'b0;
	 flip_flop <= 1'b0;
	 
      end else begin
	 flip_flop <= enable_timer;
	 enable <= flip_flop;
      end
   end

endmodule // timer

      