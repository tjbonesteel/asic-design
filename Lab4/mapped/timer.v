/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP1
// Date      : Thu Feb 11 02:01:02 2016
/////////////////////////////////////////////////////////////


module flex_counter_1 ( clk, n_rst, clear, count_enable, rollover_val, 
        count_out, rollover_flag );
  input [3:0] rollover_val;
  output [3:0] count_out;
  input clk, n_rst, clear, count_enable;
  output rollover_flag;
  wire   nr_flag, n35, n36, n37, n38, n1, n2, n3, n4, n5, n6, n7, n8, n14, n15,
         n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29,
         n30, n31, n32, n33, n34, n39, n40, n41, n42, n43, n44, n45, n46, n47,
         n48, n49, n50, n51;

  DFFSR \c_count_reg[0]  ( .D(n38), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[0]) );
  DFFSR \c_count_reg[1]  ( .D(n37), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[1]) );
  DFFSR \c_count_reg[2]  ( .D(n36), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[2]) );
  DFFSR \c_count_reg[3]  ( .D(n35), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[3]) );
  DFFSR cr_flag_reg ( .D(nr_flag), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        rollover_flag) );
  OAI21X1 U5 ( .A(n1), .B(n2), .C(n3), .Y(nr_flag) );
  NAND2X1 U6 ( .A(rollover_flag), .B(n4), .Y(n3) );
  NAND3X1 U10 ( .A(n5), .B(n6), .C(n7), .Y(n2) );
  XOR2X1 U11 ( .A(n8), .B(n14), .Y(n7) );
  XOR2X1 U12 ( .A(rollover_val[2]), .B(count_out[2]), .Y(n14) );
  NAND2X1 U13 ( .A(count_out[1]), .B(count_out[0]), .Y(n8) );
  XOR2X1 U14 ( .A(n15), .B(n16), .Y(n6) );
  XOR2X1 U15 ( .A(n17), .B(n18), .Y(n5) );
  NAND3X1 U16 ( .A(count_enable), .B(n19), .C(n20), .Y(n1) );
  INVX1 U17 ( .A(n21), .Y(n20) );
  OAI21X1 U18 ( .A(n22), .B(n15), .C(n23), .Y(n21) );
  NAND3X1 U19 ( .A(count_out[1]), .B(count_out[0]), .C(count_out[2]), .Y(n15)
         );
  XOR2X1 U20 ( .A(rollover_val[0]), .B(count_out[0]), .Y(n19) );
  OAI22X1 U21 ( .A(n17), .B(n24), .C(n25), .D(n26), .Y(n38) );
  OAI21X1 U22 ( .A(n27), .B(n24), .C(n28), .Y(n37) );
  MUX2X1 U23 ( .B(n29), .A(n30), .S(n31), .Y(n28) );
  NOR2X1 U24 ( .A(clear), .B(n25), .Y(n30) );
  NOR2X1 U25 ( .A(n26), .B(n32), .Y(n29) );
  OAI21X1 U26 ( .A(n33), .B(n24), .C(n34), .Y(n36) );
  MUX2X1 U27 ( .B(n39), .A(n40), .S(n41), .Y(n34) );
  NOR2X1 U28 ( .A(clear), .B(n42), .Y(n40) );
  NOR2X1 U29 ( .A(n26), .B(n43), .Y(n39) );
  OAI22X1 U30 ( .A(n22), .B(n24), .C(n44), .D(n26), .Y(n35) );
  NAND2X1 U31 ( .A(n24), .B(n23), .Y(n26) );
  INVX1 U32 ( .A(clear), .Y(n23) );
  XOR2X1 U33 ( .A(n45), .B(n46), .Y(n44) );
  NOR2X1 U34 ( .A(n47), .B(n22), .Y(n46) );
  NAND2X1 U35 ( .A(n42), .B(n41), .Y(n45) );
  NOR2X1 U36 ( .A(n33), .B(n47), .Y(n41) );
  INVX1 U37 ( .A(n43), .Y(n42) );
  NAND2X1 U38 ( .A(n25), .B(n31), .Y(n43) );
  NOR2X1 U39 ( .A(n27), .B(n47), .Y(n31) );
  INVX1 U40 ( .A(n48), .Y(n47) );
  INVX1 U41 ( .A(count_out[1]), .Y(n27) );
  INVX1 U42 ( .A(n32), .Y(n25) );
  NAND2X1 U43 ( .A(count_out[0]), .B(n48), .Y(n32) );
  NAND3X1 U44 ( .A(n49), .B(n50), .C(n51), .Y(n48) );
  NOR2X1 U45 ( .A(n16), .B(n18), .Y(n51) );
  XOR2X1 U46 ( .A(count_out[1]), .B(rollover_val[1]), .Y(n18) );
  XOR2X1 U47 ( .A(count_out[3]), .B(rollover_val[3]), .Y(n16) );
  XOR2X1 U48 ( .A(rollover_val[0]), .B(n17), .Y(n50) );
  INVX1 U49 ( .A(count_out[0]), .Y(n17) );
  XOR2X1 U50 ( .A(rollover_val[2]), .B(n33), .Y(n49) );
  INVX1 U51 ( .A(count_out[2]), .Y(n33) );
  INVX1 U52 ( .A(n4), .Y(n24) );
  NOR2X1 U53 ( .A(count_enable), .B(clear), .Y(n4) );
  INVX1 U54 ( .A(count_out[3]), .Y(n22) );
endmodule


module flex_counter_0 ( clk, n_rst, clear, count_enable, rollover_val, 
        count_out, rollover_flag );
  input [3:0] rollover_val;
  output [3:0] count_out;
  input clk, n_rst, clear, count_enable;
  output rollover_flag;
  wire   nr_flag, n1, n2, n3, n4, n5, n6, n7, n8, n14, n15, n16, n17, n18, n19,
         n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33,
         n34, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51,
         n52, n53, n54, n55;

  DFFSR \c_count_reg[0]  ( .D(n52), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[0]) );
  DFFSR \c_count_reg[1]  ( .D(n53), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[1]) );
  DFFSR \c_count_reg[2]  ( .D(n54), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[2]) );
  DFFSR \c_count_reg[3]  ( .D(n55), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[3]) );
  DFFSR cr_flag_reg ( .D(nr_flag), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        rollover_flag) );
  OAI21X1 U5 ( .A(n1), .B(n2), .C(n3), .Y(nr_flag) );
  NAND2X1 U6 ( .A(rollover_flag), .B(n4), .Y(n3) );
  NAND3X1 U10 ( .A(n5), .B(n6), .C(n7), .Y(n2) );
  XOR2X1 U11 ( .A(n8), .B(n14), .Y(n7) );
  XOR2X1 U12 ( .A(rollover_val[2]), .B(count_out[2]), .Y(n14) );
  NAND2X1 U13 ( .A(count_out[1]), .B(count_out[0]), .Y(n8) );
  XOR2X1 U14 ( .A(n15), .B(n16), .Y(n6) );
  XOR2X1 U15 ( .A(n17), .B(n18), .Y(n5) );
  NAND3X1 U16 ( .A(count_enable), .B(n19), .C(n20), .Y(n1) );
  INVX1 U17 ( .A(n21), .Y(n20) );
  OAI21X1 U18 ( .A(n22), .B(n15), .C(n23), .Y(n21) );
  NAND3X1 U19 ( .A(count_out[1]), .B(count_out[0]), .C(count_out[2]), .Y(n15)
         );
  XOR2X1 U20 ( .A(rollover_val[0]), .B(count_out[0]), .Y(n19) );
  OAI22X1 U21 ( .A(n17), .B(n24), .C(n25), .D(n26), .Y(n52) );
  OAI21X1 U22 ( .A(n27), .B(n24), .C(n28), .Y(n53) );
  MUX2X1 U23 ( .B(n29), .A(n30), .S(n31), .Y(n28) );
  NOR2X1 U24 ( .A(clear), .B(n25), .Y(n30) );
  NOR2X1 U25 ( .A(n26), .B(n32), .Y(n29) );
  OAI21X1 U26 ( .A(n33), .B(n24), .C(n34), .Y(n54) );
  MUX2X1 U27 ( .B(n39), .A(n40), .S(n41), .Y(n34) );
  NOR2X1 U28 ( .A(clear), .B(n42), .Y(n40) );
  NOR2X1 U29 ( .A(n26), .B(n43), .Y(n39) );
  OAI22X1 U30 ( .A(n22), .B(n24), .C(n44), .D(n26), .Y(n55) );
  NAND2X1 U31 ( .A(n24), .B(n23), .Y(n26) );
  INVX1 U32 ( .A(clear), .Y(n23) );
  XOR2X1 U33 ( .A(n45), .B(n46), .Y(n44) );
  NOR2X1 U34 ( .A(n47), .B(n22), .Y(n46) );
  NAND2X1 U35 ( .A(n42), .B(n41), .Y(n45) );
  NOR2X1 U36 ( .A(n33), .B(n47), .Y(n41) );
  INVX1 U37 ( .A(n43), .Y(n42) );
  NAND2X1 U38 ( .A(n25), .B(n31), .Y(n43) );
  NOR2X1 U39 ( .A(n27), .B(n47), .Y(n31) );
  INVX1 U40 ( .A(n48), .Y(n47) );
  INVX1 U41 ( .A(count_out[1]), .Y(n27) );
  INVX1 U42 ( .A(n32), .Y(n25) );
  NAND2X1 U43 ( .A(count_out[0]), .B(n48), .Y(n32) );
  NAND3X1 U44 ( .A(n49), .B(n50), .C(n51), .Y(n48) );
  NOR2X1 U45 ( .A(n16), .B(n18), .Y(n51) );
  XOR2X1 U46 ( .A(count_out[1]), .B(rollover_val[1]), .Y(n18) );
  XOR2X1 U47 ( .A(count_out[3]), .B(rollover_val[3]), .Y(n16) );
  XOR2X1 U48 ( .A(rollover_val[0]), .B(n17), .Y(n50) );
  INVX1 U49 ( .A(count_out[0]), .Y(n17) );
  XOR2X1 U50 ( .A(rollover_val[2]), .B(n33), .Y(n49) );
  INVX1 U51 ( .A(count_out[2]), .Y(n33) );
  INVX1 U52 ( .A(n4), .Y(n24) );
  NOR2X1 U53 ( .A(count_enable), .B(clear), .Y(n4) );
  INVX1 U54 ( .A(count_out[3]), .Y(n22) );
endmodule


module timer ( clk, n_rst, enable_timer, shift_strobe, packet_done );
  input clk, n_rst, enable_timer;
  output shift_strobe, packet_done;
  wire   enable, flip_flop;

  DFFSR flip_flop_reg ( .D(enable_timer), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        flip_flop) );
  DFFSR enable_reg ( .D(flip_flop), .CLK(clk), .R(n_rst), .S(1'b1), .Q(enable)
         );
  flex_counter_1 count1 ( .clk(clk), .n_rst(n_rst), .clear(packet_done), 
        .count_enable(enable), .rollover_val({1'b1, 1'b0, 1'b1, 1'b0}), 
        .rollover_flag(shift_strobe) );
  flex_counter_0 count2 ( .clk(clk), .n_rst(n_rst), .clear(packet_done), 
        .count_enable(shift_strobe), .rollover_val({1'b1, 1'b0, 1'b0, 1'b1}), 
        .rollover_flag(packet_done) );
endmodule

