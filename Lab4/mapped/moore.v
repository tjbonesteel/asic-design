/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP1
// Date      : Wed Feb 10 23:13:28 2016
/////////////////////////////////////////////////////////////


module moore ( clk, n_rst, i, o );
  input clk, n_rst, i;
  output o;
  wire   N88, N89, N90, N91, n45, n46, n47, n48, n49, n50, n51, n52;
  wire   [31:0] c_state;
  wire   [31:0] n_state;

  LATCH \n_state_reg[0]  ( .CLK(N88), .D(N89), .Q(n_state[0]) );
  DFFSR \c_state_reg[0]  ( .D(n_state[0]), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        c_state[0]) );
  LATCH \n_state_reg[1]  ( .CLK(N88), .D(N90), .Q(n_state[1]) );
  DFFSR \c_state_reg[1]  ( .D(n_state[1]), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        c_state[1]) );
  LATCH \n_state_reg[2]  ( .CLK(N88), .D(N91), .Q(n_state[2]) );
  DFFSR \c_state_reg[2]  ( .D(n_state[2]), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        c_state[2]) );
  NOR2X1 U80 ( .A(n45), .B(n46), .Y(o) );
  NAND2X1 U81 ( .A(n47), .B(n48), .Y(n46) );
  NOR2X1 U82 ( .A(n49), .B(n48), .Y(N91) );
  MUX2X1 U83 ( .B(n49), .A(n50), .S(c_state[1]), .Y(N90) );
  NAND2X1 U84 ( .A(n47), .B(n45), .Y(n50) );
  NAND3X1 U85 ( .A(c_state[0]), .B(n45), .C(i), .Y(n49) );
  NOR2X1 U86 ( .A(c_state[0]), .B(n51), .Y(N89) );
  MUX2X1 U87 ( .B(n45), .A(n48), .S(i), .Y(n51) );
  INVX1 U88 ( .A(c_state[2]), .Y(n45) );
  NAND3X1 U89 ( .A(n47), .B(n48), .C(n52), .Y(N88) );
  NOR2X1 U90 ( .A(i), .B(c_state[2]), .Y(n52) );
  INVX1 U91 ( .A(c_state[1]), .Y(n48) );
  INVX1 U92 ( .A(c_state[0]), .Y(n47) );
endmodule

