/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP1
// Date      : Thu Feb 11 01:58:43 2016
/////////////////////////////////////////////////////////////


module rcu ( clk, n_rst, start_bit_detected, packet_done, framing_error, 
        sbc_clear, sbc_enable, load_buffer, enable_timer );
  input clk, n_rst, start_bit_detected, packet_done, framing_error;
  output sbc_clear, sbc_enable, load_buffer, enable_timer;
  wire   N34, N35, N36, n4, enable_timer, n21, n22, n23, n24, n25, n26, n27,
         n28, n29, n30, n31, n32, n33, n34;
  wire   [2:0] c_state;
  wire   [2:0] n_state;
  assign sbc_clear = enable_timer;

  LATCH \n_state_reg[0]  ( .CLK(N34), .D(N35), .Q(n_state[0]) );
  DFFSR \c_state_reg[0]  ( .D(n_state[0]), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        c_state[0]) );
  LATCH \n_state_reg[1]  ( .CLK(N34), .D(N36), .Q(n_state[1]) );
  DFFSR \c_state_reg[1]  ( .D(n_state[1]), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        c_state[1]) );
  LATCH \n_state_reg[2]  ( .CLK(N34), .D(n4), .Q(n_state[2]) );
  DFFSR \c_state_reg[2]  ( .D(n_state[2]), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        c_state[2]) );
  NOR2X1 U28 ( .A(n21), .B(n22), .Y(sbc_enable) );
  NAND2X1 U29 ( .A(n23), .B(n24), .Y(n22) );
  INVX1 U30 ( .A(n25), .Y(n4) );
  NAND3X1 U31 ( .A(c_state[1]), .B(c_state[0]), .C(n26), .Y(n25) );
  NOR2X1 U32 ( .A(framing_error), .B(c_state[2]), .Y(n26) );
  NOR2X1 U33 ( .A(n24), .B(n27), .Y(load_buffer) );
  NAND2X1 U34 ( .A(n23), .B(n21), .Y(n27) );
  NOR2X1 U35 ( .A(n23), .B(n28), .Y(enable_timer) );
  NAND2X1 U36 ( .A(n21), .B(n24), .Y(n28) );
  INVX1 U37 ( .A(n_state[2]), .Y(n24) );
  INVX1 U38 ( .A(n_state[1]), .Y(n21) );
  INVX1 U39 ( .A(n_state[0]), .Y(n23) );
  NOR2X1 U40 ( .A(c_state[2]), .B(n29), .Y(N36) );
  MUX2X1 U41 ( .B(c_state[1]), .A(n30), .S(c_state[0]), .Y(n29) );
  NOR2X1 U42 ( .A(c_state[1]), .B(n31), .Y(n30) );
  INVX1 U43 ( .A(packet_done), .Y(n31) );
  NOR2X1 U44 ( .A(c_state[2]), .B(n32), .Y(N35) );
  MUX2X1 U45 ( .B(n33), .A(n34), .S(c_state[0]), .Y(n32) );
  NOR2X1 U46 ( .A(packet_done), .B(c_state[1]), .Y(n34) );
  OR2X1 U47 ( .A(start_bit_detected), .B(c_state[1]), .Y(n33) );
  OAI21X1 U48 ( .A(c_state[1]), .B(c_state[0]), .C(c_state[2]), .Y(N34) );
endmodule

