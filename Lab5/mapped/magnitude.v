/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP1
// Date      : Tue Feb 23 20:26:11 2016
/////////////////////////////////////////////////////////////


module magnitude_DW01_inc_0 ( A, SUM );
  input [15:0] A;
  output [15:0] SUM;

  wire   [15:2] carry;

  HAX1 U1_1_14 ( .A(A[14]), .B(carry[14]), .YC(carry[15]), .YS(SUM[14]) );
  HAX1 U1_1_13 ( .A(A[13]), .B(carry[13]), .YC(carry[14]), .YS(SUM[13]) );
  HAX1 U1_1_12 ( .A(A[12]), .B(carry[12]), .YC(carry[13]), .YS(SUM[12]) );
  HAX1 U1_1_11 ( .A(A[11]), .B(carry[11]), .YC(carry[12]), .YS(SUM[11]) );
  HAX1 U1_1_10 ( .A(A[10]), .B(carry[10]), .YC(carry[11]), .YS(SUM[10]) );
  HAX1 U1_1_9 ( .A(A[9]), .B(carry[9]), .YC(carry[10]), .YS(SUM[9]) );
  HAX1 U1_1_8 ( .A(A[8]), .B(carry[8]), .YC(carry[9]), .YS(SUM[8]) );
  HAX1 U1_1_7 ( .A(A[7]), .B(carry[7]), .YC(carry[8]), .YS(SUM[7]) );
  HAX1 U1_1_6 ( .A(A[6]), .B(carry[6]), .YC(carry[7]), .YS(SUM[6]) );
  HAX1 U1_1_5 ( .A(A[5]), .B(carry[5]), .YC(carry[6]), .YS(SUM[5]) );
  HAX1 U1_1_4 ( .A(A[4]), .B(carry[4]), .YC(carry[5]), .YS(SUM[4]) );
  HAX1 U1_1_3 ( .A(A[3]), .B(carry[3]), .YC(carry[4]), .YS(SUM[3]) );
  HAX1 U1_1_2 ( .A(A[2]), .B(carry[2]), .YC(carry[3]), .YS(SUM[2]) );
  HAX1 U1_1_1 ( .A(A[1]), .B(A[0]), .YC(carry[2]), .YS(SUM[1]) );
  INVX2 U1 ( .A(A[0]), .Y(SUM[0]) );
  XOR2X1 U2 ( .A(carry[15]), .B(A[15]), .Y(SUM[15]) );
endmodule


module magnitude ( in, out );
  input [16:0] in;
  output [15:0] out;
  wire   N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29, N30, N31,
         N32, N33, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44,
         n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58,
         n59, n60, n61, n62, n63, n64;

  magnitude_DW01_inc_0 add_15 ( .A({n49, n50, n51, n52, n53, n54, n55, n56, 
        n57, n58, n59, n60, n61, n62, n63, n64}), .SUM({N33, N32, N31, N30, 
        N29, N28, N27, N26, N25, N24, N23, N22, N21, N20, N19, N18}) );
  INVX1 U50 ( .A(n33), .Y(out[9]) );
  MUX2X1 U51 ( .B(in[9]), .A(N27), .S(in[16]), .Y(n33) );
  INVX1 U52 ( .A(n34), .Y(out[8]) );
  MUX2X1 U53 ( .B(in[8]), .A(N26), .S(in[16]), .Y(n34) );
  INVX1 U54 ( .A(n35), .Y(out[7]) );
  MUX2X1 U55 ( .B(in[7]), .A(N25), .S(in[16]), .Y(n35) );
  INVX1 U56 ( .A(n36), .Y(out[6]) );
  MUX2X1 U57 ( .B(in[6]), .A(N24), .S(in[16]), .Y(n36) );
  INVX1 U58 ( .A(n37), .Y(out[5]) );
  MUX2X1 U59 ( .B(in[5]), .A(N23), .S(in[16]), .Y(n37) );
  INVX1 U60 ( .A(n38), .Y(out[4]) );
  MUX2X1 U61 ( .B(in[4]), .A(N22), .S(in[16]), .Y(n38) );
  INVX1 U62 ( .A(n39), .Y(out[3]) );
  MUX2X1 U63 ( .B(in[3]), .A(N21), .S(in[16]), .Y(n39) );
  INVX1 U64 ( .A(n40), .Y(out[2]) );
  MUX2X1 U65 ( .B(in[2]), .A(N20), .S(in[16]), .Y(n40) );
  INVX1 U66 ( .A(n41), .Y(out[1]) );
  MUX2X1 U67 ( .B(in[1]), .A(N19), .S(in[16]), .Y(n41) );
  INVX1 U68 ( .A(n42), .Y(out[15]) );
  MUX2X1 U69 ( .B(in[15]), .A(N33), .S(in[16]), .Y(n42) );
  INVX1 U70 ( .A(n43), .Y(out[14]) );
  MUX2X1 U71 ( .B(in[14]), .A(N32), .S(in[16]), .Y(n43) );
  INVX1 U72 ( .A(n44), .Y(out[13]) );
  MUX2X1 U73 ( .B(in[13]), .A(N31), .S(in[16]), .Y(n44) );
  INVX1 U74 ( .A(n45), .Y(out[12]) );
  MUX2X1 U75 ( .B(in[12]), .A(N30), .S(in[16]), .Y(n45) );
  INVX1 U76 ( .A(n46), .Y(out[11]) );
  MUX2X1 U77 ( .B(in[11]), .A(N29), .S(in[16]), .Y(n46) );
  INVX1 U78 ( .A(n47), .Y(out[10]) );
  MUX2X1 U79 ( .B(in[10]), .A(N28), .S(in[16]), .Y(n47) );
  INVX1 U80 ( .A(n48), .Y(out[0]) );
  MUX2X1 U81 ( .B(in[0]), .A(N18), .S(in[16]), .Y(n48) );
  INVX1 U82 ( .A(in[15]), .Y(n49) );
  INVX1 U83 ( .A(in[14]), .Y(n50) );
  INVX1 U84 ( .A(in[13]), .Y(n51) );
  INVX1 U85 ( .A(in[12]), .Y(n52) );
  INVX1 U86 ( .A(in[11]), .Y(n53) );
  INVX1 U87 ( .A(in[10]), .Y(n54) );
  INVX1 U88 ( .A(in[9]), .Y(n55) );
  INVX1 U89 ( .A(in[8]), .Y(n56) );
  INVX1 U90 ( .A(in[7]), .Y(n57) );
  INVX1 U91 ( .A(in[6]), .Y(n58) );
  INVX1 U92 ( .A(in[5]), .Y(n59) );
  INVX1 U93 ( .A(in[4]), .Y(n60) );
  INVX1 U94 ( .A(in[3]), .Y(n61) );
  INVX1 U95 ( .A(in[2]), .Y(n62) );
  INVX1 U96 ( .A(in[1]), .Y(n63) );
  INVX1 U97 ( .A(in[0]), .Y(n64) );
endmodule

