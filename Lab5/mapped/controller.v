/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP1
// Date      : Tue Feb 23 20:20:34 2016
/////////////////////////////////////////////////////////////


module controller ( clk, n_reset, dr, lc, overflow, cnt_up, clear, modwait, op, 
        src1, src2, dest, err );
  output [2:0] op;
  output [3:0] src1;
  output [3:0] src2;
  output [3:0] dest;
  input clk, n_reset, dr, lc, overflow;
  output cnt_up, clear, modwait, err;
  wire   n_modwait, n170, n171, n172, n173, n174, n175, n176, n177, n178, n179,
         n180, n181, n182, n183, n184, n185, n186, n187, n188, n189, n190,
         n191, n192, n193, n194, n195, n196, n197, n198, n199, n200, n201,
         n202, n203, n204, n205, n206, n207, n208, n209, n210, n211, n212,
         n213, n214, n215, n216, n217, n218, n219, n220, n221, n222, n223,
         n224, n225, n226, n227, n228, n229, n230, n231, n232, n233, n234,
         n235, n236, n237, n238, n239, n240, n241, n242, n243, n244, n245,
         n246, n247, n248, n249, n250, n251, n252, n253, n254, n255, n256,
         n257, n258, n259, n260, n261, n262, n263, n264, n265, n266, n267,
         n268, n269, n270, n271, n272, n273, n274, n275, n276, n277, n278,
         n279, n280, n281, n282, n283, n284, n285, n286, n287, n288, n289,
         n290, n291, n292, n293, n294, n295, n296, n297, n298, n299, n300,
         n301, n302;
  wire   [4:0] c_state;
  wire   [4:0] n_state;
  assign src1[3] = 1'b0;

  DFFSR c_modwait_reg ( .D(n_modwait), .CLK(clk), .R(n_reset), .S(1'b1), .Q(
        modwait) );
  DFFSR \c_state_reg[0]  ( .D(n_state[0]), .CLK(clk), .R(n_reset), .S(1'b1), 
        .Q(c_state[0]) );
  DFFSR \c_state_reg[1]  ( .D(n_state[1]), .CLK(clk), .R(n_reset), .S(1'b1), 
        .Q(c_state[1]) );
  DFFSR \c_state_reg[2]  ( .D(n_state[2]), .CLK(clk), .R(n_reset), .S(1'b1), 
        .Q(c_state[2]) );
  DFFSR \c_state_reg[4]  ( .D(n_state[4]), .CLK(clk), .R(n_reset), .S(1'b1), 
        .Q(c_state[4]) );
  DFFSR \c_state_reg[3]  ( .D(n_state[3]), .CLK(clk), .R(n_reset), .S(1'b1), 
        .Q(c_state[3]) );
  NAND3X1 U192 ( .A(n170), .B(n171), .C(n172), .Y(src2[1]) );
  NAND2X1 U193 ( .A(n173), .B(n174), .Y(src1[1]) );
  NAND3X1 U194 ( .A(n175), .B(n174), .C(n176), .Y(src1[0]) );
  NAND3X1 U195 ( .A(n171), .B(n177), .C(n178), .Y(op[2]) );
  NOR2X1 U196 ( .A(n179), .B(cnt_up), .Y(n178) );
  INVX1 U197 ( .A(src2[2]), .Y(n177) );
  NAND3X1 U198 ( .A(n174), .B(n180), .C(n172), .Y(src2[2]) );
  INVX1 U199 ( .A(n181), .Y(n172) );
  NAND3X1 U200 ( .A(n182), .B(n183), .C(n184), .Y(n181) );
  NAND2X1 U201 ( .A(n185), .B(n186), .Y(op[1]) );
  INVX1 U202 ( .A(dest[3]), .Y(n186) );
  INVX1 U203 ( .A(n187), .Y(op[0]) );
  OR2X1 U204 ( .A(n188), .B(n189), .Y(n_state[4]) );
  NAND3X1 U205 ( .A(n190), .B(n191), .C(n192), .Y(n189) );
  OAI21X1 U206 ( .A(n193), .B(n194), .C(overflow), .Y(n192) );
  INVX1 U207 ( .A(n182), .Y(n193) );
  OAI21X1 U208 ( .A(err), .B(n195), .C(n196), .Y(n190) );
  NAND3X1 U209 ( .A(n183), .B(n197), .C(n198), .Y(n188) );
  INVX1 U210 ( .A(src2[3]), .Y(n197) );
  NAND3X1 U211 ( .A(n199), .B(n200), .C(n201), .Y(n_state[3]) );
  NOR2X1 U212 ( .A(n202), .B(n203), .Y(n201) );
  OAI21X1 U213 ( .A(overflow), .B(n204), .C(n205), .Y(n203) );
  INVX1 U214 ( .A(n206), .Y(n202) );
  NOR2X1 U215 ( .A(n207), .B(n208), .Y(n200) );
  AOI21X1 U216 ( .A(n209), .B(c_state[3]), .C(n210), .Y(n199) );
  NAND3X1 U217 ( .A(n211), .B(n212), .C(n213), .Y(n_state[2]) );
  NOR2X1 U218 ( .A(n214), .B(n215), .Y(n213) );
  OAI21X1 U219 ( .A(lc), .B(n206), .C(n216), .Y(n215) );
  OR2X1 U220 ( .A(n208), .B(src1[2]), .Y(n214) );
  NAND3X1 U221 ( .A(n217), .B(n218), .C(n180), .Y(src1[2]) );
  AOI22X1 U222 ( .A(n209), .B(c_state[2]), .C(n195), .D(n196), .Y(n212) );
  AND2X1 U223 ( .A(n173), .B(n219), .Y(n211) );
  INVX1 U224 ( .A(n220), .Y(n173) );
  NAND3X1 U225 ( .A(n221), .B(n171), .C(n175), .Y(n220) );
  OR2X1 U226 ( .A(n222), .B(n223), .Y(n_state[1]) );
  NAND3X1 U227 ( .A(n224), .B(n219), .C(n225), .Y(n223) );
  AOI22X1 U228 ( .A(n209), .B(c_state[1]), .C(n226), .D(lc), .Y(n225) );
  NOR2X1 U229 ( .A(n227), .B(n228), .Y(n219) );
  OAI21X1 U230 ( .A(dr), .B(n229), .C(n230), .Y(n228) );
  OAI21X1 U231 ( .A(n231), .B(n232), .C(overflow), .Y(n230) );
  NAND2X1 U232 ( .A(n182), .B(n183), .Y(n232) );
  OAI21X1 U233 ( .A(n233), .B(n234), .C(n204), .Y(n227) );
  NAND3X1 U234 ( .A(n235), .B(n218), .C(n176), .Y(n222) );
  NOR2X1 U235 ( .A(n207), .B(n179), .Y(n176) );
  INVX1 U236 ( .A(n170), .Y(n179) );
  INVX1 U237 ( .A(n217), .Y(n207) );
  OR2X1 U238 ( .A(n236), .B(n237), .Y(n_state[0]) );
  NAND3X1 U239 ( .A(n238), .B(n224), .C(n239), .Y(n237) );
  INVX1 U240 ( .A(n240), .Y(n239) );
  OAI22X1 U241 ( .A(n184), .B(overflow), .C(n191), .D(n241), .Y(n240) );
  INVX1 U242 ( .A(n209), .Y(n191) );
  NOR2X1 U243 ( .A(n242), .B(n243), .Y(n209) );
  NAND3X1 U244 ( .A(n244), .B(n185), .C(n206), .Y(n243) );
  AOI21X1 U245 ( .A(n245), .B(n246), .C(n226), .Y(n206) );
  INVX1 U246 ( .A(n247), .Y(n244) );
  NAND3X1 U247 ( .A(n248), .B(n187), .C(n184), .Y(n242) );
  NOR2X1 U248 ( .A(n249), .B(n250), .Y(n187) );
  NAND3X1 U249 ( .A(n221), .B(n182), .C(n251), .Y(n250) );
  AND2X1 U250 ( .A(n175), .B(n252), .Y(n251) );
  NAND2X1 U251 ( .A(n253), .B(n254), .Y(n182) );
  NAND3X1 U252 ( .A(n183), .B(n217), .C(n255), .Y(n249) );
  NOR2X1 U253 ( .A(dest[3]), .B(n256), .Y(n255) );
  NAND2X1 U254 ( .A(n253), .B(n246), .Y(n183) );
  NOR2X1 U255 ( .A(n231), .B(n194), .Y(n184) );
  INVX1 U256 ( .A(n204), .Y(n194) );
  NAND2X1 U257 ( .A(n257), .B(n258), .Y(n204) );
  INVX1 U258 ( .A(n198), .Y(n231) );
  NAND2X1 U259 ( .A(n259), .B(n258), .Y(n198) );
  INVX1 U260 ( .A(n260), .Y(n224) );
  NAND3X1 U261 ( .A(n174), .B(n261), .C(n262), .Y(n260) );
  AOI21X1 U262 ( .A(n210), .B(n234), .C(cnt_up), .Y(n262) );
  INVX1 U263 ( .A(n248), .Y(n210) );
  NAND3X1 U264 ( .A(c_state[3]), .B(c_state[0]), .C(n263), .Y(n248) );
  MUX2X1 U265 ( .B(n226), .A(n264), .S(lc), .Y(n238) );
  OAI21X1 U266 ( .A(n265), .B(n266), .C(n233), .Y(n264) );
  NOR2X1 U267 ( .A(n265), .B(n267), .Y(n226) );
  NAND3X1 U268 ( .A(n175), .B(n180), .C(n268), .Y(n236) );
  AOI21X1 U269 ( .A(dr), .B(n247), .C(n208), .Y(n268) );
  NAND2X1 U270 ( .A(n229), .B(n233), .Y(n247) );
  NAND3X1 U271 ( .A(n246), .B(n269), .C(n270), .Y(n233) );
  NAND3X1 U272 ( .A(n271), .B(n272), .C(n273), .Y(n_modwait) );
  AOI22X1 U273 ( .A(n274), .B(modwait), .C(n275), .D(lc), .Y(n273) );
  NOR2X1 U274 ( .A(n241), .B(n276), .Y(n275) );
  AND2X1 U275 ( .A(c_state[4]), .B(n277), .Y(n274) );
  OAI21X1 U276 ( .A(n269), .B(n278), .C(n279), .Y(n277) );
  OAI21X1 U277 ( .A(n280), .B(n281), .C(n279), .Y(n272) );
  OAI22X1 U278 ( .A(overflow), .B(n282), .C(n196), .D(n283), .Y(n281) );
  MUX2X1 U279 ( .B(n284), .A(n257), .S(c_state[2]), .Y(n283) );
  INVX1 U280 ( .A(dr), .Y(n196) );
  MUX2X1 U281 ( .B(c_state[1]), .A(n246), .S(c_state[2]), .Y(n282) );
  NAND2X1 U282 ( .A(n285), .B(n276), .Y(n280) );
  OAI21X1 U283 ( .A(c_state[1]), .B(lc), .C(n286), .Y(n285) );
  NOR2X1 U284 ( .A(c_state[4]), .B(c_state[0]), .Y(n286) );
  MUX2X1 U285 ( .B(n287), .A(n245), .S(c_state[1]), .Y(n271) );
  OAI21X1 U286 ( .A(n234), .B(n288), .C(n289), .Y(n287) );
  NOR2X1 U287 ( .A(n290), .B(n258), .Y(n289) );
  NAND2X1 U288 ( .A(c_state[2]), .B(n291), .Y(n288) );
  INVX1 U289 ( .A(lc), .Y(n234) );
  INVX1 U290 ( .A(n229), .Y(err) );
  NAND2X1 U291 ( .A(n253), .B(n257), .Y(n229) );
  INVX1 U292 ( .A(n292), .Y(n253) );
  NAND3X1 U293 ( .A(c_state[4]), .B(n279), .C(c_state[2]), .Y(n292) );
  NAND3X1 U294 ( .A(n293), .B(n294), .C(n205), .Y(dest[3]) );
  NAND3X1 U295 ( .A(n185), .B(n293), .C(n295), .Y(dest[2]) );
  AND2X1 U296 ( .A(n217), .B(n294), .Y(n295) );
  NAND2X1 U297 ( .A(n290), .B(n257), .Y(n217) );
  INVX1 U298 ( .A(n208), .Y(n293) );
  NOR2X1 U299 ( .A(n296), .B(n267), .Y(n208) );
  INVX1 U300 ( .A(n254), .Y(n267) );
  NOR2X1 U301 ( .A(n195), .B(src2[3]), .Y(n185) );
  NAND3X1 U302 ( .A(n205), .B(n175), .C(n297), .Y(dest[1]) );
  NOR2X1 U303 ( .A(src2[3]), .B(n256), .Y(n297) );
  NAND3X1 U304 ( .A(n170), .B(n298), .C(n174), .Y(src2[3]) );
  NAND2X1 U305 ( .A(n246), .B(n258), .Y(n174) );
  INVX1 U306 ( .A(src2[0]), .Y(n298) );
  NAND2X1 U307 ( .A(n171), .B(n180), .Y(src2[0]) );
  NAND2X1 U308 ( .A(n254), .B(n258), .Y(n180) );
  INVX1 U309 ( .A(n299), .Y(n258) );
  NAND3X1 U310 ( .A(n269), .B(n279), .C(c_state[4]), .Y(n299) );
  NAND2X1 U311 ( .A(n245), .B(n259), .Y(n171) );
  NAND2X1 U312 ( .A(n257), .B(n245), .Y(n170) );
  INVX1 U313 ( .A(n265), .Y(n245) );
  NAND3X1 U314 ( .A(c_state[2]), .B(n291), .C(c_state[3]), .Y(n265) );
  NOR2X1 U315 ( .A(n284), .B(c_state[0]), .Y(n257) );
  NAND2X1 U316 ( .A(n290), .B(n246), .Y(n175) );
  INVX1 U317 ( .A(n266), .Y(n246) );
  NOR2X1 U318 ( .A(clear), .B(n300), .Y(n205) );
  NAND3X1 U319 ( .A(n301), .B(n294), .C(n302), .Y(dest[0]) );
  AND2X1 U320 ( .A(n221), .B(n216), .Y(n302) );
  INVX1 U321 ( .A(n300), .Y(n216) );
  NOR2X1 U322 ( .A(n296), .B(n266), .Y(n300) );
  NAND2X1 U323 ( .A(n241), .B(n284), .Y(n266) );
  INVX1 U324 ( .A(c_state[1]), .Y(n284) );
  NAND3X1 U325 ( .A(n269), .B(n291), .C(c_state[3]), .Y(n296) );
  NAND3X1 U326 ( .A(c_state[0]), .B(n279), .C(n263), .Y(n221) );
  NAND3X1 U327 ( .A(c_state[3]), .B(n241), .C(n263), .Y(n294) );
  NOR2X1 U328 ( .A(n256), .B(n195), .Y(n301) );
  INVX1 U329 ( .A(n235), .Y(n195) );
  NAND3X1 U330 ( .A(n254), .B(n269), .C(n270), .Y(n235) );
  INVX1 U331 ( .A(n218), .Y(n256) );
  NAND2X1 U332 ( .A(n290), .B(n254), .Y(n218) );
  NOR2X1 U333 ( .A(n241), .B(c_state[1]), .Y(n254) );
  INVX1 U334 ( .A(n252), .Y(cnt_up) );
  NAND3X1 U335 ( .A(n241), .B(n279), .C(n263), .Y(n252) );
  INVX1 U336 ( .A(n276), .Y(n263) );
  NAND3X1 U337 ( .A(n269), .B(n291), .C(c_state[1]), .Y(n276) );
  INVX1 U338 ( .A(c_state[4]), .Y(n291) );
  INVX1 U339 ( .A(c_state[2]), .Y(n269) );
  INVX1 U340 ( .A(c_state[3]), .Y(n279) );
  INVX1 U341 ( .A(c_state[0]), .Y(n241) );
  INVX1 U342 ( .A(n261), .Y(clear) );
  NAND2X1 U343 ( .A(n290), .B(n259), .Y(n261) );
  INVX1 U344 ( .A(n278), .Y(n259) );
  NAND2X1 U345 ( .A(c_state[1]), .B(c_state[0]), .Y(n278) );
  AND2X1 U346 ( .A(n270), .B(c_state[2]), .Y(n290) );
  NOR2X1 U347 ( .A(c_state[3]), .B(c_state[4]), .Y(n270) );
endmodule

