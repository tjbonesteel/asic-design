/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP1
// Date      : Tue Feb 23 20:21:43 2016
/////////////////////////////////////////////////////////////


module flex_counter_NUM_CNT_BITS10_DW01_inc_0 ( A, SUM );
  input [9:0] A;
  output [9:0] SUM;

  wire   [9:2] carry;

  HAX1 U1_1_8 ( .A(A[8]), .B(carry[8]), .YC(carry[9]), .YS(SUM[8]) );
  HAX1 U1_1_7 ( .A(A[7]), .B(carry[7]), .YC(carry[8]), .YS(SUM[7]) );
  HAX1 U1_1_6 ( .A(A[6]), .B(carry[6]), .YC(carry[7]), .YS(SUM[6]) );
  HAX1 U1_1_5 ( .A(A[5]), .B(carry[5]), .YC(carry[6]), .YS(SUM[5]) );
  HAX1 U1_1_4 ( .A(A[4]), .B(carry[4]), .YC(carry[5]), .YS(SUM[4]) );
  HAX1 U1_1_3 ( .A(A[3]), .B(carry[3]), .YC(carry[4]), .YS(SUM[3]) );
  HAX1 U1_1_2 ( .A(A[2]), .B(carry[2]), .YC(carry[3]), .YS(SUM[2]) );
  HAX1 U1_1_1 ( .A(A[1]), .B(A[0]), .YC(carry[2]), .YS(SUM[1]) );
  INVX2 U1 ( .A(A[0]), .Y(SUM[0]) );
  XOR2X1 U2 ( .A(carry[9]), .B(A[9]), .Y(SUM[9]) );
endmodule


module flex_counter_NUM_CNT_BITS10_DW01_inc_1 ( A, SUM );
  input [10:0] A;
  output [10:0] SUM;

  wire   [10:2] carry;

  HAX1 U1_1_9 ( .A(A[9]), .B(carry[9]), .YC(SUM[10]), .YS(SUM[9]) );
  HAX1 U1_1_8 ( .A(A[8]), .B(carry[8]), .YC(carry[9]), .YS(SUM[8]) );
  HAX1 U1_1_7 ( .A(A[7]), .B(carry[7]), .YC(carry[8]), .YS(SUM[7]) );
  HAX1 U1_1_6 ( .A(A[6]), .B(carry[6]), .YC(carry[7]), .YS(SUM[6]) );
  HAX1 U1_1_5 ( .A(A[5]), .B(carry[5]), .YC(carry[6]), .YS(SUM[5]) );
  HAX1 U1_1_4 ( .A(A[4]), .B(carry[4]), .YC(carry[5]), .YS(SUM[4]) );
  HAX1 U1_1_3 ( .A(A[3]), .B(carry[3]), .YC(carry[4]), .YS(SUM[3]) );
  HAX1 U1_1_2 ( .A(A[2]), .B(carry[2]), .YC(carry[3]), .YS(SUM[2]) );
  HAX1 U1_1_1 ( .A(A[1]), .B(A[0]), .YC(carry[2]), .YS(SUM[1]) );
  INVX2 U1 ( .A(A[0]), .Y(SUM[0]) );
endmodule


module flex_counter_NUM_CNT_BITS10 ( clk, n_rst, clear, count_enable, 
        rollover_val, count_out, rollover_flag );
  input [9:0] rollover_val;
  output [9:0] count_out;
  input clk, n_rst, clear, count_enable;
  output rollover_flag;
  wire   nr_flag, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15, N23, N24,
         N25, N26, N27, N28, N29, N30, N31, N32, N36, N37, N38, N39, N40, N41,
         N42, N43, N44, N45, n1, n3, n4, n5, n6, n7, n8, n20, n21, n22, n23,
         n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37,
         n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51,
         n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65,
         n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79,
         n80;

  DFFSR \c_count_reg[0]  ( .D(n80), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[0]) );
  DFFSR \c_count_reg[1]  ( .D(n79), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[1]) );
  DFFSR \c_count_reg[2]  ( .D(n78), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[2]) );
  DFFSR \c_count_reg[3]  ( .D(n77), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[3]) );
  DFFSR \c_count_reg[4]  ( .D(n76), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[4]) );
  DFFSR \c_count_reg[5]  ( .D(n75), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[5]) );
  DFFSR \c_count_reg[6]  ( .D(n74), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[6]) );
  DFFSR \c_count_reg[7]  ( .D(n73), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[7]) );
  DFFSR \c_count_reg[8]  ( .D(n72), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[8]) );
  DFFSR \c_count_reg[9]  ( .D(n71), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[9]) );
  DFFSR cr_flag_reg ( .D(nr_flag), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        rollover_flag) );
  flex_counter_NUM_CNT_BITS10_DW01_inc_0 add_53_aco ( .A({N45, N44, N43, N42, 
        N41, N40, N39, N38, N37, N36}), .SUM({N32, N31, N30, N29, N28, N27, 
        N26, N25, N24, N23}) );
  flex_counter_NUM_CNT_BITS10_DW01_inc_1 add_39 ( .A({1'b0, count_out}), .SUM(
        {N15, N14, N13, N12, N11, N10, N9, N8, N7, N6, N5}) );
  INVX1 U5 ( .A(n1), .Y(nr_flag) );
  AOI22X1 U7 ( .A(n3), .B(n4), .C(rollover_flag), .D(n5), .Y(n1) );
  NOR2X1 U17 ( .A(n6), .B(n7), .Y(n4) );
  NAND3X1 U18 ( .A(n8), .B(n20), .C(n21), .Y(n7) );
  XNOR2X1 U19 ( .A(rollover_val[2]), .B(N7), .Y(n21) );
  XNOR2X1 U20 ( .A(rollover_val[3]), .B(N8), .Y(n20) );
  XNOR2X1 U21 ( .A(rollover_val[1]), .B(N6), .Y(n8) );
  NAND3X1 U22 ( .A(n22), .B(count_enable), .C(n23), .Y(n6) );
  NOR2X1 U23 ( .A(clear), .B(N15), .Y(n23) );
  XNOR2X1 U24 ( .A(rollover_val[0]), .B(N5), .Y(n22) );
  NOR2X1 U25 ( .A(n24), .B(n25), .Y(n3) );
  NAND3X1 U26 ( .A(n26), .B(n27), .C(n28), .Y(n25) );
  XNOR2X1 U27 ( .A(rollover_val[8]), .B(N13), .Y(n28) );
  XNOR2X1 U28 ( .A(rollover_val[9]), .B(N14), .Y(n27) );
  XNOR2X1 U29 ( .A(rollover_val[7]), .B(N12), .Y(n26) );
  NAND3X1 U30 ( .A(n29), .B(n30), .C(n31), .Y(n24) );
  XNOR2X1 U31 ( .A(rollover_val[5]), .B(N10), .Y(n31) );
  XNOR2X1 U32 ( .A(rollover_val[6]), .B(N11), .Y(n30) );
  XNOR2X1 U33 ( .A(rollover_val[4]), .B(N9), .Y(n29) );
  OAI21X1 U34 ( .A(n32), .B(n33), .C(n34), .Y(n71) );
  NAND2X1 U35 ( .A(N32), .B(n35), .Y(n34) );
  OAI21X1 U36 ( .A(n32), .B(n36), .C(n37), .Y(n72) );
  NAND2X1 U37 ( .A(N31), .B(n35), .Y(n37) );
  OAI21X1 U38 ( .A(n32), .B(n38), .C(n39), .Y(n73) );
  NAND2X1 U39 ( .A(N30), .B(n35), .Y(n39) );
  OAI21X1 U40 ( .A(n32), .B(n40), .C(n41), .Y(n74) );
  NAND2X1 U41 ( .A(N29), .B(n35), .Y(n41) );
  OAI21X1 U42 ( .A(n32), .B(n42), .C(n43), .Y(n75) );
  NAND2X1 U43 ( .A(N28), .B(n35), .Y(n43) );
  OAI21X1 U44 ( .A(n32), .B(n44), .C(n45), .Y(n76) );
  NAND2X1 U45 ( .A(N27), .B(n35), .Y(n45) );
  OAI21X1 U46 ( .A(n32), .B(n46), .C(n47), .Y(n77) );
  NAND2X1 U47 ( .A(N26), .B(n35), .Y(n47) );
  OAI21X1 U48 ( .A(n32), .B(n48), .C(n49), .Y(n78) );
  NAND2X1 U49 ( .A(N25), .B(n35), .Y(n49) );
  OAI21X1 U50 ( .A(n32), .B(n50), .C(n51), .Y(n79) );
  NAND2X1 U51 ( .A(N24), .B(n35), .Y(n51) );
  OAI21X1 U52 ( .A(n32), .B(n52), .C(n53), .Y(n80) );
  NAND2X1 U53 ( .A(N23), .B(n35), .Y(n53) );
  NOR2X1 U54 ( .A(n5), .B(clear), .Y(n35) );
  INVX1 U55 ( .A(n5), .Y(n32) );
  NOR2X1 U56 ( .A(clear), .B(count_enable), .Y(n5) );
  NOR2X1 U57 ( .A(n54), .B(n33), .Y(N45) );
  INVX1 U58 ( .A(count_out[9]), .Y(n33) );
  NOR2X1 U59 ( .A(n54), .B(n36), .Y(N44) );
  INVX1 U60 ( .A(count_out[8]), .Y(n36) );
  NOR2X1 U61 ( .A(n54), .B(n38), .Y(N43) );
  INVX1 U62 ( .A(count_out[7]), .Y(n38) );
  NOR2X1 U63 ( .A(n54), .B(n40), .Y(N42) );
  INVX1 U64 ( .A(count_out[6]), .Y(n40) );
  NOR2X1 U65 ( .A(n54), .B(n42), .Y(N41) );
  INVX1 U66 ( .A(count_out[5]), .Y(n42) );
  NOR2X1 U67 ( .A(n54), .B(n44), .Y(N40) );
  INVX1 U68 ( .A(count_out[4]), .Y(n44) );
  NOR2X1 U69 ( .A(n54), .B(n46), .Y(N39) );
  INVX1 U70 ( .A(count_out[3]), .Y(n46) );
  NOR2X1 U71 ( .A(n54), .B(n48), .Y(N38) );
  INVX1 U72 ( .A(count_out[2]), .Y(n48) );
  NOR2X1 U73 ( .A(n54), .B(n50), .Y(N37) );
  INVX1 U74 ( .A(count_out[1]), .Y(n50) );
  NOR2X1 U75 ( .A(n54), .B(n52), .Y(N36) );
  INVX1 U76 ( .A(count_out[0]), .Y(n52) );
  AND2X1 U77 ( .A(n55), .B(n56), .Y(n54) );
  NOR2X1 U78 ( .A(n57), .B(n58), .Y(n56) );
  NAND2X1 U79 ( .A(n59), .B(n60), .Y(n58) );
  XNOR2X1 U80 ( .A(count_out[3]), .B(rollover_val[3]), .Y(n60) );
  XNOR2X1 U81 ( .A(count_out[9]), .B(rollover_val[9]), .Y(n59) );
  NAND3X1 U82 ( .A(n61), .B(n62), .C(n63), .Y(n57) );
  XNOR2X1 U83 ( .A(count_out[1]), .B(rollover_val[1]), .Y(n63) );
  XNOR2X1 U84 ( .A(count_out[2]), .B(rollover_val[2]), .Y(n62) );
  XNOR2X1 U85 ( .A(count_out[0]), .B(rollover_val[0]), .Y(n61) );
  NOR2X1 U86 ( .A(n64), .B(n65), .Y(n55) );
  NAND2X1 U87 ( .A(n66), .B(n67), .Y(n65) );
  XNOR2X1 U88 ( .A(count_out[7]), .B(rollover_val[7]), .Y(n67) );
  XNOR2X1 U89 ( .A(count_out[8]), .B(rollover_val[8]), .Y(n66) );
  NAND3X1 U90 ( .A(n68), .B(n69), .C(n70), .Y(n64) );
  XNOR2X1 U91 ( .A(count_out[5]), .B(rollover_val[5]), .Y(n70) );
  XNOR2X1 U92 ( .A(count_out[6]), .B(rollover_val[6]), .Y(n69) );
  XNOR2X1 U93 ( .A(count_out[4]), .B(rollover_val[4]), .Y(n68) );
endmodule


module counter ( clk, n_reset, cnt_up, clear, one_k_samples );
  input clk, n_reset, cnt_up, clear;
  output one_k_samples;


  flex_counter_NUM_CNT_BITS10 count ( .clk(clk), .n_rst(n_reset), .clear(clear), .count_enable(cnt_up), .rollover_val({1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b0, 
        1'b1, 1'b0, 1'b0, 1'b0}), .rollover_flag(one_k_samples) );
endmodule

