// $Id: $
// File name:   sync_low.sv
// Created:     1/26/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: Reset to logic low synchronizer

module sync_low
  (
   input wire clk,
   input wire n_rst,
   input wire async_in,
   output reg sync_out
   );

   reg 	      P, Q;

   always_ff @ (posedge clk, negedge n_rst) begin : sync_l
     if(n_rst == 1'b0) begin
	P <= 0;
	Q <=0;

     end else begin
	P <= async_in;
	Q <= Q;

     end
   end // always_ff @ (posedge clk, negedge n_rst)
   assign sync_out = Q;
   
endmodule // sync_low
