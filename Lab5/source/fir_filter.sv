// $Id: $
// File name:   fir_filter.sv
// Created:     2/16/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: fir filter top level design

module fir_filter
  (
   input wire clk,
   input wire n_reset,
   input wire [15:0] sample_data,
   input wire [15:0] fir_coefficient,
   input wire load_coeff,
   input wire data_ready,
   output wire one_k_samples,
   output wire modwait,
   output wire [15:0] fir_out,
   output wire  err
   );
   

   logic [2:0] op;
   logic [3:0] src1;
   logic [3:0] src2;
   logic [3:0] dest;
   logic       overflow;
   logic       cnt_up;
   logic [16:0] outreg_data;
   logic      syncdr;
   logic      synclc;
   logic      clear;
   
   


   magnitude Mag
     (
      .in(outreg_data),
      .out(fir_out)
      );

   sync Sync1
     (
      .clk(clk),
      .n_rst(n_reset),
      .async_in(data_ready),
      .sync_out(syncdr)
      );

   sync Sync2
     (
      .clk(clk),
      .n_rst(n_reset),
      .async_in(load_coeff),
      .sync_out(synclc)
      );
   

   controller Controller
     (
      .clk(clk),
      .n_reset(n_reset),
      .dr(syncdr),
      .lc(synclc),
      .overflow(overflow),
      .cnt_up(cnt_up),
      .clear(clear),
      .op(op),
      .src1(src1),
      .src2(src2),
      .dest(dest),
      .modwait(modwait),
      .err(err)
      );

   datapath Datapath
     (
      .clk(clk),
      .n_reset(n_reset),
      .op(op),
      .src1(src1),
      .src2(src2),
      .dest(dest),
      .overflow(overflow),
      .ext_data1(sample_data),
      .ext_data2(fir_coefficient),
      .outreg_data(outreg_data)
      );
   
   counter Counter
     (
      .clk(clk),
      .n_reset(n_reset),
      .cnt_up(cnt_up),
      .clear(clear),
      .one_k_samples(one_k_samples)
      );
   
   
   
  

  endmodule // fir_filter
