// $Id: $
// File name:   controller.sv
// Created:     2/16/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Controller datapath-based design


module controller
  (
   input wire clk,
   input wire n_reset,
   input wire dr,
   input wire lc,
   input wire overflow,
   output reg cnt_up,
   output reg clear,
   output wire modwait,
   output reg [2:0] op,
   output reg [3:0] src1,
   output reg [3:0] src2,
   output reg [3:0] dest,
   output reg err
   );

   reg 	      n_modwait;
   reg 	      c_modwait;
   
   

   typedef enum bit [4:0] {idle, store, zero, sort1, sort2, sort3, sort4, lc1, lc2, lc3, lc4, wait1, wait2, wait3, mul1, mul2, mul3, mul4, add1, add2, sub1, sub2, eidle} state_type;

   state_type c_state, n_state;

   always_ff @ (posedge clk, negedge n_reset) begin
      if(n_reset == 0) begin
	 c_state <= idle;
	 c_modwait <= 0;

      end
      else begin
	 c_state <= n_state;
	 c_modwait <= n_modwait;
	 
      end
   end // always_ff @ (posedge clk, negedge n_reset)
   
   assign modwait = c_modwait;
   

   always_comb begin
      cnt_up = 0;
      op = 3'b000;
      src1 = 0;
      src2 = 0;
      dest = 0;
      err = 0;
      clear = 0;
      n_state = c_state;
      n_modwait = c_modwait;


      case(c_state)

	idle: begin
	   
	   if(lc == 1) begin
	      
	      n_modwait = 1;
	      n_state = lc1;
	   end else if(dr == 1)begin
	      n_modwait = 1;
	      n_state = store;
	   end else begin
	      n_modwait = 0;
	      n_state = idle;
	   end

	   cnt_up = 0;
	   op = 3'b000;
	   src1 = 0;
	   src2 = 0;
	   dest = 0;
	   err = 0;
	   clear = 0;
	   
	   
	end // case: idle


	lc1: begin
	   n_state = wait1;
	   clear = 1;
	   op = 3'b011;
	   src1 = 0;
	   src2 = 0;
	   dest = 10;
	   err = 0;
	   cnt_up = 0;
	   n_modwait = 0;
	end

	wait1: begin
	   if(lc == 1) begin
	      n_state = lc2;
	      n_modwait = 1;
	   end else begin
	      n_state = wait1;
	      n_modwait = 0;
	   end
	   cnt_up = 0;
	   clear = 0;
	   op = 3'b000;
	   err = 0;
	end // case: wait1
	

	lc2: begin
	   n_modwait = 0;
	   n_state = wait2;
	   clear = 0;
	   op = 3'b011;
	   dest = 11;
	   cnt_up = 0;
	   
	   err = 0;
	end // case: lc2

	wait2: begin
	   if(lc == 1) begin
	      n_state = lc3;
	      n_modwait = 1;
	   end else begin
	      n_state = wait2;
	      n_modwait = 0;
	   end
	   cnt_up = 0;
	   clear = 0;
	   op = 3'b000;
	   err = 0;
	end // case: wait2

	lc3: begin
	   n_state = wait3;
	   clear = 0;
	   op = 3'b011;
	   dest = 12;
	   err = 0;
	   cnt_up = 0;
	   
	   n_modwait = 0;
	end // case: lc3

	wait3: begin
	   if(lc == 1) begin
	      n_state = lc4;
	      n_modwait = 1;
	   end else begin
	      n_state = wait3;
	      n_modwait = 0;
	   end
	   cnt_up = 0;
	   clear = 0;
	   op = 3'b000;
	   err = 0;
	end // case: wait2


	lc4: begin
	   n_state = idle;
	   clear = 0;
	   op = 3'b011;
	   dest = 13;
	   err = 0;
	   n_modwait = 0;
	   cnt_up = 0;
	   
	end
	
	store: begin
	   if(dr == 1) begin
	      n_state = zero;
	      n_modwait = 1;
	   end else begin
	      n_state = eidle;
	      n_modwait = 0;
	   end

	   cnt_up = 0;
	   op = 3'b010;
	   src1 = 0;
	   src2 = 0;
	   dest = 5;
	   err = 0;
	end // case: store

	zero: begin
	   n_state = sort1;
	   cnt_up = 1;
	   clear = 0;
	   n_modwait = 1;
	   op = 3'b101;
	   src1 = 0;
	   src2 = 0;
	   dest = 0;
	   err = 0;
	end // case: zero
	
	   

	sort1: begin
	   n_state = sort2;
	   cnt_up = 0;
	   n_modwait = 1;
	   op = 3'b001;
	   src1 = 2;
	   src2 = 0;
	   dest = 1;
	   err = 0;
	   clear = 0;
	   
	end
	
	sort2: begin
	   n_state = sort3;
	   cnt_up = 0;
	   n_modwait = 1;
	   op = 3'b001;
	   src1 = 3;
	   src2 = 0;
	   dest = 2;
	   err = 0;
	   clear = 0;
	   
	end
	
	sort3: begin
	   n_state = sort4;
	   cnt_up = 0;
	   n_modwait = 1;
	   op = 3'b001;
	   src1 = 4;
	   src2 = 0;
	   dest = 3;
	   err = 0;
	   clear = 0;
	end // case: sort3
	
	sort4: begin
	   n_state = mul1;
	   cnt_up = 0;
	   n_modwait = 1;
	   op = 3'b001;
	   src1 = 5;
	   src2 = 0;
	   dest = 4;
	   err = 0;
	   clear = 0;
	end // case: sort4
	

	mul1: begin  //Sample 4 * F3
	   n_modwait = 1;
	   n_state = add1;
	   cnt_up = 0;
	   op = 3'b110;
	   src1 = 1;
	   src2 = 10;
	   dest = 6;
	   err = 0;
	end

	add1: begin //add small positive coefficient
	   if(overflow == 0) begin
	      n_modwait = 1;
	      n_state = mul2;
	   end else begin
	      n_modwait = 0;
	      n_state = eidle;
	   end

	   cnt_up = 0;
	   op = 3'b100;
	   src1 = 0;
	   src2 = 6;
	   dest = 0;
	   err = 0;
	end // case: add1

	mul2: begin  //Sample 3 * F2
	   n_modwait = 1;
	   n_state = sub1;
	   cnt_up = 0;
	   op = 3'b110;
	   src1 = 2;
	   src2 = 11;
	   dest = 6;
	   err = 0;
	   clear = 0;
	   
	end

	sub1: begin //add large negative coefficient
	   if(overflow == 0) begin
	      n_modwait = 1;
	      n_state = mul3;
	   end else begin
	      n_modwait = 0;
	      n_state = eidle;
	   end

	   cnt_up = 0;
	   op = 3'b101;
	   src1 = 0;
	   src2 = 6;
	   dest = 0;
	   err = 0;
	   clear = 0;
	   
	end // case: sub1

	
	mul3: begin  //Sample 2 * F1
	   n_modwait = 1;
	   n_state = add2;
	   cnt_up = 0;
	   op = 3'b110;
	   src1 = 3;
	   src2 = 12;
	   dest = 6;
	   err = 0;
	   clear = 0;
	   
	end

	add2: begin //add small negative coefficient
	   if(overflow == 0) begin
	      n_modwait = 1;
	      n_state = mul4;
	   end else begin
	      n_modwait = 0;
	      n_state = eidle;
	   end

	   cnt_up = 0;
	   op = 3'b100;
	   src1 = 0;
	   src2 = 6;
	   dest = 0;
	   err = 0;
	   clear = 0;
	   
	end // case: add2
	
	
	mul4: begin  //Sample 1 * F0
	   n_modwait = 1;
	   n_state = sub2;
	   cnt_up = 0;
	   op = 3'b110;
	   src1 = 4;
	   src2 = 13;
	   dest = 6;
	   err = 0;
	   clear = 0;
	   
	end

	sub2: begin //add small negative coefficient
	   if(overflow)begin
	      n_modwait = 0;
	      n_state = eidle;
	   end else begin
	      n_modwait = 0;
	      n_state = idle;
	   end

	   cnt_up = 0;
	   op = 3'b101;
	   src1 = 0;
	   src2 = 6;
	   dest = 0;
	   err = 1'b0;
	   clear = 1'b0;
	   
	end // case: sub2

	eidle: begin
	   if(dr == 1)begin
	      n_modwait = 1;
	      n_state = store;
	   end else begin
	      n_modwait = 0;
	      n_state = eidle;
	   end

	   cnt_up = 0;
	   op = 3'b000;
	   src1 = 0;
	   src2 = 0;
	   dest = 0;
	   err = 1;
	end // case: eidle
	
      endcase // case (c_state)
      
   end // always_comb begin



   

   
endmodule // controller


	   
