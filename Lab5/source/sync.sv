// $Id: $
// File name:   sync.sv
// Created:     2/16/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Sync_low from Lab2 renamed to sync

module sync
  (
   input wire clk,
   input wire n_rst,
   input wire async_in,
   output reg sync_out
   );

   reg 	      P,Q;

   always_ff @ (posedge clk, negedge n_rst) begin : sync_l
     if(1'b0 == n_rst) begin
	P <= 0;
	Q <=0;

     end else begin
	P <= async_in;
	Q <= P;

     end
   end // always_ff @ (posedge clk, negedge n_rst)

   assign sync_out = Q;
   
endmodule // sync
