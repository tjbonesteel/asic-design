// $Id: $
// File name:   flex_stp_sr.sv
// Created:     2/4/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: N-Bit Serial to Parallel shift register

module flex_stp_sr
  #(
    parameter NUM_BITS = 4,
    parameter SHIFT_MSB = 1
   )
  (
   input wire clk, n_rst, shift_enable, serial_in,
   output reg [(NUM_BITS-1):0]parallel_out
 
   );

   reg [(NUM_BITS-1):0]       n_state;

   always_ff @(posedge clk, negedge n_rst)begin
      if(n_rst == 0)
	parallel_out <= '1;
      else
	parallel_out <= n_state;
   end

   always_comb begin
      n_state = parallel_out;
      if(shift_enable ==1) begin
	 if(SHIFT_MSB ==1)
	   n_state = {parallel_out[(NUM_BITS-2):0], serial_in};
	 else
	   n_state = {serial_in, parallel_out[(NUM_BITS-1):1]};
      end
   end
endmodule // flex_stp_sr

   
	 
		   