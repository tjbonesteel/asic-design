// $Id: $
// File name:   edge_detect.sv
// Created:     2/23/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Detects rising and falling edge

module edge_detect
  (
   input wire clk,
   input wire n_rst,
   input wire d_plus,
   output reg d_edge

   );

   logic 	      Q0;
   logic 	      Q1;
   
   always_ff @ (posedge clk, negedge n_rst) begin
      if(n_rst == 0) begin
	 Q0 <= 1;
	 d_edge <= 0;
	 
      end
      else begin
	 Q0 <= d_plus;
	 d_edge <= Q1;
	 
      end
      

   end // always_ff @ (posedge clk, negedge n_rst)

   always_comb begin
     Q1  = (Q0 ^ d_plus);
   end
   

endmodule // edge_detect
