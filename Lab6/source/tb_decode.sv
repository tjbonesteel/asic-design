// $Id: $
// File name:   tb_decode.sv
// Created:     2/23/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: test bench for decode module

`timescale 1ns/100ps
module tb_decode();

   localparam CLK_PERIOD = 2.5;

   localparam CHECK_DELAY = 1;
   
   reg tb_clk;
   reg tb_n_rst;
   reg tb_d_plus;
   reg tb_shift_enable;
   reg tb_eop;
   reg tb_d_orig;

   decode DUT(
	      .clk(tb_clk),
	      .n_rst(tb_n_rst),
	      .d_plus(tb_d_plus),
	      .shift_enable(tb_shift_enable),
	      .eop(tb_eop),
	      .d_orig(tb_d_orig)
	      );
   always
     begin
	tb_clk = 1'b0;
	#(CLK_PERIOD/2.0);
	tb_clk = 1'b1;
	#(CLK_PERIOD/2.0);
   end
   
   initial
     begin
	tb_d_plus = 0;
	tb_shift_enable = 0;
	tb_eop =0;
	
	
		
	tb_n_rst = 1;
	@(posedge tb_clk);
	tb_n_rst = 0;
	@(posedge tb_clk);
	tb_n_rst = 1;
	
	@(posedge tb_clk);
	@(posedge tb_clk);
	@(posedge tb_clk);
	@(posedge tb_clk);
	@(posedge tb_clk);
	
	@(posedge tb_clk);
        tb_d_plus = 1;
	@(posedge tb_clk);
	@(posedge tb_clk);
	@(posedge tb_clk);
	tb_shift_enable = 1;
	@(posedge tb_clk);
	tb_shift_enable = 0;
	@(posedge tb_clk);
	@(posedge tb_clk);
	@(posedge tb_clk);
	@(posedge tb_clk);
	@(posedge tb_clk);
	tb_d_plus = 1;
	@(posedge tb_clk);
	@(posedge tb_clk);
	@(posedge tb_clk);
	@(posedge tb_clk);
	tb_shift_enable = 1;
	@(posedge tb_clk);
	tb_shift_enable = 0;
	@(posedge tb_clk);
	@(posedge tb_clk);
	tb_d_plus = 0;
	@(posedge tb_clk);
	@(posedge tb_clk);
	tb_shift_enable = 1;
	@(posedge tb_clk);
	tb_shift_enable = 0;
	@(posedge tb_clk);
end
	
endmodule // tb_decode

