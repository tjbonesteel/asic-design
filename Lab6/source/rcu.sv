// $Id: $
// File name:   rcu.sv
// Created:     2/29/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Receiver control unit block

module rcu
  (
   input wire clk,
   input wire n_rst,
   input wire d_edge,
   input wire eop,
   input wire shift_enable,
   input wire [7:0]rcv_data,
   input wire byte_received,
   output logic rcving,
   output logic w_enable,
   output logic r_error
   );


   typedef enum bit[3:0] {idle, rcv1, rcv2, en_fifo, sync, comp, e_wait, e_idle, e_sync, match} states;
   
   states c_state, n_state;

   always_ff @ (posedge clk, negedge n_rst) begin
      if(n_rst == 0) begin
	 c_state <= idle;
      end else begin
	 c_state <= n_state;
      end
   end


   always_comb begin
      n_state = c_state;
      r_error = 0;
      rcving = 0;
      w_enable = 0;

      case(c_state)

	 idle: begin
	    rcving = 0;
	    r_error = 0;
	    w_enable = 0;
	    if(d_edge) begin
	       n_state = rcv1;
	    end else begin
	       n_state = idle;
	    end
	 end


	rcv1: begin
	   rcving = 1;
	   r_error = 0;
	   w_enable = 0;
	   if(byte_received) begin
	      n_state = sync;
	   end else begin
	      n_state = rcv1;
	   end
	end

	sync: begin
	   rcving = 1;
	   
	    r_error = 0;
	    w_enable = 0;
	   if(rcv_data == 8'b10000000) begin
	      n_state = rcv2;
	   end else begin
	      n_state = e_sync;
	   end
	   
	end

	rcv2: begin
	    r_error = 0;
	    rcving = 1;
	   w_enable = 0;
	   if(byte_received) begin
	      n_state = en_fifo;
	   end else begin
	      n_state = rcv2;
	   end
	   if(eop && shift_enable) begin
	      n_state = e_wait;
	   end
	end
	   
	

	en_fifo: begin
	    r_error = 0;
	    rcving = 1;
	    w_enable = 1;
	   n_state = match;
	   
	   
	end
	

	match: begin
	   if(shift_enable) begin
	      if(eop) begin
		 n_state = comp;
	      end else begin
		 n_state = rcv2;
	      end
	   end else begin
	      n_state = match;
	   end
	    r_error = 0;
	    rcving = 1;
	    w_enable = 0;
	   
	end
	comp: begin
	   if(d_edge) begin
	      n_state = idle;
	   end else begin
	      n_state = comp;
	   end
	    r_error = 0;
	    rcving = 0;
	    w_enable = 0;
	end
	
	e_sync: begin
	   if(eop && shift_enable) begin
	      n_state = e_wait;
	   end else begin
	      n_state = e_sync;
	   end
	    r_error = 1;
	   rcving = 1;
	   w_enable = 0;
	   
	end

	e_wait: begin
	   if (d_edge) begin
	      n_state = e_idle;
	   end else begin
	      n_state = e_wait;
	   end
	    r_error = 1;
	    rcving = 0;
	    w_enable = 0;
	   
	end



	e_idle: begin
	   if(d_edge) begin
	      n_state = rcv1;
	   end else begin
	      n_state = e_idle;
	   end
	    r_error = 1;
	    rcving = 0;
	    w_enable = 0;
	end

      endcase // case (state)
   end // always_comb begin
   
	 
endmodule // rcu

   
