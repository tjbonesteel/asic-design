// $Id: $
// File name:   sync_high.sv
// Created:     1/26/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: Reset to logic low synchronizer

module sync_high
  (
   input wire clk,
   input wire n_rst,
   input wire async_in,
   output reg sync_out
   );

   reg 	      Q1;
   reg 	      Q2;
   

   always_ff @ (posedge clk, negedge n_rst) begin : sync_l
     if(1'b0 == n_rst) begin
	Q1 <= 1;
	Q2 <= 1;

     end else begin
	Q1 <= async_in;
	Q2 <= Q1;

     end
   end // always_ff @ (posedge clk, negedge n_rst)

   assign sync_out = Q2;
   
endmodule // sync_low
