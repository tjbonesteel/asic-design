// $Id: $
// File name:   tb_eop_detect.sv
// Created:     2/23/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: test bench for eop detect
`timescale 1ns /100ps

module tb_eop_detect();

   localparam CLK_PERIOD = 10;
   localparam CHECK_DELAY = 3;

   reg tb_d_plus;
   reg tb_d_minus;
   reg tb_eop;


   eop_detect DUT (
		   .d_plus(tb_d_plus),
		   .d_minus(tb_d_minus),
		   .eop(tb_eop)
		   );

  
   initial
     begin
	tb_d_plus = 1'b1;
	tb_d_minus = 1'b0;
	#(CHECK_DELAY);

	
	tb_d_plus = 1'b0;
	tb_d_minus = 1'b0;
	#(CHECK_DELAY);

	
	tb_d_plus = 1'b0;
	tb_d_minus = 1'b1;
	#(CHECK_DELAY);

	
	tb_d_plus = 1'b1;
	tb_d_minus = 1'b1;
	#(CHECK_DELAY);
	tb_d_plus = 0;
	tb_d_minus = 0;

	#(CHECK_DELAY);
	

     end // initial begin
endmodule // tb_eop_detect
