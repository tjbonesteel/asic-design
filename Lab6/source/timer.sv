// $Id: $
// File name:   timer.sv
// Created:     3/8/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Timer block

module timer
  (
   input wire clk,
   input wire n_rst,
   input wire d_edge,
   input wire rcving,
   output wire shift_enable,
   output wire byte_received


   );

   reg [3:0]   count1;
   reg [3:0]   count2;
   

   flex_counter #(4) C1
     (
      .clk(clk),
      .n_rst(n_rst),
      .clear(~rcving || d_edge),
      .count_enable(rcving),
      .rollover_val(4'd8),
      .count_out(count1)
      );
   assign shift_enable = (count1 == 3);
   
   
   flex_counter #(4) C2
     (
      .clk(clk),
      .n_rst(n_rst),
      .clear(byte_received || !rcving),
      .count_enable(shift_enable),
      .rollover_val(4'd8),
      .rollover_flag(byte_received),
      .count_out(count2)
      );

endmodule // timer

   
