// $Id: $
// File name:   tb_edge_detect.sv
// Created:     2/23/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Test Bench for edge detector module

`timescale 1ns / 10ps
module tb_edge_detect ();
   //Parameters for test bench
   localparam CLK_PERIOD = 10;
   localparam CHECK_DELAY = 3;
   localparam NUM_CNT_BITS = 4;


   reg clk;
   reg tb_n_rst;
   reg tb_d_plus;
   reg tb_d_edge;


   edge_detect DUT (
		    .clk(clk), 
		    .n_rst(tb_n_rst), 
		    .d_plus(tb_d_plus), 
		    .d_edge(tb_d_edge)
		    );


   always begin
      clk = 1'b0;
      #(CLK_PERIOD/2.0);
      clk = 1'b1;
      #(CLK_PERIOD/2.0);
   end

   initial
     begin
        tb_n_rst = 1;
        tb_d_plus = 0;
	@(posedge clk);
	tb_n_rst = 0;
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	tb_n_rst = 1;
	@(posedge clk);
	@(posedge clk);
	
        tb_d_plus = 1;
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	tb_d_plus = 0;
	
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	@(posedge clk);
	

	#(CHECK_DELAY);
	
	/*
	tb_n_rst = 1'b1;
	tb_d_plus = 1'b0;

	//@(posedge tb_clk);
	//@(posedge tb_clk);
	//@(posedge tb_clk);

	assert (!tb_d_edge) begin
	   $info("Test case 1: PASSED");
	end else begin
	   $error("Test case 1: FAIL!!");
	end // UNMATCHED !!

	tb_d_plus = 1'b1;
	tb_n_rst = 1'b1;

	@(posedge tb_clk);
	#(CHECK_DELAY);
	tb_d_plus = 1'b0;

	@(posedge tb_clk);

	#(CHECK_DELAY);

	assert (tb_d_edge) begin
	   $info("Test case 1: PASSED");
	end else begin
	   $error("Test case 1: FAIL!!");
	end // UNMATCHED !!

	tb_d_plus = 1'b0;
	tb_n_rst = 1'b1;

	@(posedge tb_clk);
	#(CHECK_DELAY);
	tb_d_plus = 1'b1;

	@(posedge tb_clk);

	#(CHECK_DELAY);

	assert (tb_d_edge) begin
	   $info("Test case 1: PASSED");
	end else begin
	   $error("Test case 1: FAIL!!");
	end // UNMATCHED !!
	
	*/
     end
	
	   
endmodule

	
