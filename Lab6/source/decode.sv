// $Id: $
// File name:   decode.sv
// Created:     2/22/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Decode block

module decode
  (
   input wire clk,
   input wire n_rst,
   input wire d_plus,
   input wire shift_enable,
   input wire eop,
   output reg d_orig
   );

   logic      reg1;
   logic      reg2;
   logic      n_dplus;
   
   always_ff @(posedge clk, negedge n_rst)begin
      if(n_rst == 1'b0)begin
	 n_dplus <= 1;
	 reg2 <= 1;
      end else begin
	 reg2 <= reg1;
	 n_dplus <= d_plus;
      end
      
   end // always_ff @ (posedge clk, negedge n_rst, posedge shift_enable)

   assign d_orig = ~(n_dplus ^ reg2);
   
   always_comb begin
      if(shift_enable) begin
	 reg1 = d_plus;
	 
	 if(eop) begin
	    reg1 = 1'b1;
	 end else begin
	    reg1 = d_plus;
	 end	 
      end else begin
	 reg1 = reg2;
      end // else: !if(shift_enable)
   end // always_comb begin
   
      
endmodule // decode

