/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP1
// Date      : Mon Apr 11 21:47:03 2016
/////////////////////////////////////////////////////////////


module rcu ( clk, n_rst, d_edge, eop, shift_enable, rcv_data, byte_received, 
        rcving, w_enable, r_error );
  input [7:0] rcv_data;
  input clk, n_rst, d_edge, eop, shift_enable, byte_received;
  output rcving, w_enable, r_error;
  wire   N57, N61, N63, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68,
         n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n80, n81, n82,
         n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n94, n95, n96,
         n97, n98, n99, n100, n101, n102, n103, n104, n105, n106, n107, n108,
         n109, n110, n111, n112;
  wire   [3:0] c_state;
  wire   [3:0] n_state;

  DFFSR \c_state_reg[3]  ( .D(n_state[3]), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        c_state[3]) );
  DFFSR \c_state_reg[2]  ( .D(n_state[2]), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        c_state[2]) );
  DFFSR \c_state_reg[1]  ( .D(n_state[1]), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        c_state[1]) );
  DFFSR \c_state_reg[0]  ( .D(n_state[0]), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        c_state[0]) );
  OR2X2 U66 ( .A(n104), .B(n105), .Y(n58) );
  NAND3X1 U67 ( .A(n90), .B(n101), .C(n91), .Y(n59) );
  OAI21X1 U68 ( .A(n91), .B(n90), .C(n59), .Y(n60) );
  NAND3X1 U69 ( .A(n60), .B(n98), .C(d_edge), .Y(n66) );
  NOR2X1 U70 ( .A(c_state[3]), .B(c_state[2]), .Y(n80) );
  NAND3X1 U71 ( .A(n80), .B(c_state[1]), .C(N57), .Y(n65) );
  AOI22X1 U72 ( .A(n80), .B(n112), .C(n110), .D(c_state[3]), .Y(n61) );
  NAND2X1 U73 ( .A(c_state[2]), .B(c_state[3]), .Y(n81) );
  NAND2X1 U74 ( .A(n61), .B(n81), .Y(n63) );
  NOR2X1 U75 ( .A(n91), .B(d_edge), .Y(n83) );
  NAND2X1 U76 ( .A(n95), .B(n90), .Y(n62) );
  OAI21X1 U77 ( .A(n63), .B(n62), .C(c_state[0]), .Y(n64) );
  NAND3X1 U78 ( .A(n66), .B(n65), .C(n64), .Y(n_state[0]) );
  XNOR2X1 U79 ( .A(c_state[3]), .B(c_state[1]), .Y(n67) );
  OAI22X1 U80 ( .A(n91), .B(n90), .C(n93), .D(n67), .Y(n70) );
  AOI22X1 U81 ( .A(byte_received), .B(n80), .C(N63), .D(c_state[3]), .Y(n68)
         );
  OAI21X1 U82 ( .A(c_state[1]), .B(n68), .C(n95), .Y(n69) );
  AOI22X1 U83 ( .A(n70), .B(n101), .C(n69), .D(c_state[0]), .Y(n71) );
  NAND2X1 U84 ( .A(n71), .B(n81), .Y(n_state[2]) );
  NOR2X1 U85 ( .A(c_state[0]), .B(c_state[1]), .Y(n72) );
  AOI22X1 U86 ( .A(n72), .B(c_state[2]), .C(c_state[1]), .D(n91), .Y(n78) );
  OAI22X1 U87 ( .A(n58), .B(n91), .C(c_state[0]), .D(c_state[2]), .Y(n77) );
  NAND2X1 U88 ( .A(n111), .B(c_state[0]), .Y(n73) );
  OAI21X1 U89 ( .A(N63), .B(c_state[0]), .C(n73), .Y(n75) );
  NAND2X1 U90 ( .A(n91), .B(n90), .Y(n74) );
  OAI21X1 U91 ( .A(n75), .B(n74), .C(c_state[3]), .Y(n76) );
  OAI21X1 U92 ( .A(n78), .B(n77), .C(n76), .Y(n_state[3]) );
  OAI22X1 U93 ( .A(n93), .B(c_state[2]), .C(n91), .D(n58), .Y(n79) );
  NAND2X1 U94 ( .A(n92), .B(n81), .Y(n82) );
  OAI21X1 U95 ( .A(n94), .B(n82), .C(n90), .Y(n88) );
  NAND2X1 U96 ( .A(c_state[3]), .B(n91), .Y(n86) );
  NAND2X1 U97 ( .A(N61), .B(c_state[0]), .Y(n85) );
  OAI21X1 U98 ( .A(n83), .B(c_state[3]), .C(c_state[1]), .Y(n84) );
  OAI21X1 U99 ( .A(n86), .B(n85), .C(n84), .Y(n87) );
  AOI21X1 U100 ( .A(n88), .B(n101), .C(n87), .Y(n89) );
  INVX2 U101 ( .A(c_state[1]), .Y(n90) );
  INVX2 U102 ( .A(c_state[2]), .Y(n91) );
  INVX2 U103 ( .A(n80), .Y(n92) );
  INVX2 U104 ( .A(N63), .Y(n93) );
  INVX2 U105 ( .A(n79), .Y(n94) );
  INVX2 U106 ( .A(n89), .Y(n_state[1]) );
  INVX2 U107 ( .A(n83), .Y(n95) );
  NOR2X1 U108 ( .A(n96), .B(n97), .Y(w_enable) );
  NAND2X1 U109 ( .A(c_state[1]), .B(c_state[0]), .Y(n97) );
  NAND2X1 U110 ( .A(n91), .B(n98), .Y(n96) );
  AOI21X1 U111 ( .A(c_state[3]), .B(c_state[1]), .C(n99), .Y(rcving) );
  XNOR2X1 U112 ( .A(c_state[2]), .B(n100), .Y(n99) );
  NAND3X1 U113 ( .A(n90), .B(n98), .C(n101), .Y(n100) );
  MUX2X1 U114 ( .B(n102), .A(n103), .S(n98), .Y(r_error) );
  INVX1 U115 ( .A(c_state[3]), .Y(n98) );
  NAND2X1 U116 ( .A(c_state[2]), .B(c_state[1]), .Y(n103) );
  NAND3X1 U117 ( .A(n90), .B(n91), .C(n101), .Y(n102) );
  INVX1 U118 ( .A(c_state[0]), .Y(n101) );
  INVX1 U119 ( .A(N61), .Y(n110) );
  NAND3X1 U120 ( .A(rcv_data[7]), .B(n106), .C(n107), .Y(n105) );
  NOR2X1 U121 ( .A(rcv_data[2]), .B(rcv_data[1]), .Y(n107) );
  INVX1 U122 ( .A(rcv_data[0]), .Y(n106) );
  NAND2X1 U123 ( .A(n108), .B(n109), .Y(n104) );
  NOR2X1 U124 ( .A(rcv_data[6]), .B(rcv_data[5]), .Y(n109) );
  NOR2X1 U125 ( .A(rcv_data[4]), .B(rcv_data[3]), .Y(n108) );
  NOR2X1 U126 ( .A(n111), .B(eop), .Y(N61) );
  INVX1 U127 ( .A(shift_enable), .Y(n111) );
  NOR2X1 U128 ( .A(n112), .B(N63), .Y(N57) );
  AND2X1 U129 ( .A(eop), .B(shift_enable), .Y(N63) );
  INVX1 U130 ( .A(byte_received), .Y(n112) );
endmodule

