/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP1
// Date      : Tue Mar  8 10:39:51 2016
/////////////////////////////////////////////////////////////


module shift_register ( clk, n_rst, shift_enable, d_orig, rcv_data );
  output [7:0] rcv_data;
  input clk, n_rst, shift_enable, d_orig;

  tri   clk;
  tri   n_rst;
  tri   shift_enable;
  tri   d_orig;
  tri   [7:0] rcv_data;

  flex_stp_sr DUT ( .clk(clk), .n_rst(n_rst), .shift_enable(shift_enable), 
        .serial_in(d_orig), .parallel_out(rcv_data) );
endmodule

