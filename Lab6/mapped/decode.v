/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP1
// Date      : Tue Mar  8 10:59:58 2016
/////////////////////////////////////////////////////////////


module decode ( clk, n_rst, d_plus, shift_enable, eop, d_orig );
  input clk, n_rst, d_plus, shift_enable, eop;
  output d_orig;
  wire   n_d_orig, reg1, reg2, n6, n7;

  DFFSR reg1_reg ( .D(reg2), .CLK(clk), .R(1'b1), .S(n_rst), .Q(reg1) );
  DFFSR n_d_orig_reg ( .D(d_plus), .CLK(clk), .R(1'b1), .S(n_rst), .Q(n_d_orig) );
  INVX1 U9 ( .A(n6), .Y(reg2) );
  XOR2X1 U10 ( .A(n6), .B(n_d_orig), .Y(d_orig) );
  MUX2X1 U11 ( .B(reg1), .A(n7), .S(shift_enable), .Y(n6) );
  OR2X1 U12 ( .A(d_plus), .B(eop), .Y(n7) );
endmodule

