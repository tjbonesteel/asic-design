/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP1
// Date      : Tue Mar  8 10:39:57 2016
/////////////////////////////////////////////////////////////


module sync_high ( clk, n_rst, async_in, sync_out );
  input clk, n_rst, async_in;
  output sync_out;
  wire   Q;

  DFFSR Q_reg ( .D(async_in), .CLK(clk), .R(1'b1), .S(n_rst), .Q(Q) );
  DFFSR sync_out_reg ( .D(Q), .CLK(clk), .R(1'b1), .S(n_rst), .Q(sync_out) );
endmodule

