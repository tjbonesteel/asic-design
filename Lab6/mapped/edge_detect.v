/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP1
// Date      : Tue Mar  8 10:39:20 2016
/////////////////////////////////////////////////////////////


module edge_detect ( clk, n_rst, d_plus, d_edge );
  input clk, n_rst, d_plus;
  output d_edge;
  wire   Q0, Q1;

  DFFSR Q0_reg ( .D(d_plus), .CLK(clk), .R(1'b1), .S(n_rst), .Q(Q0) );
  DFFSR d_edge_reg ( .D(Q1), .CLK(clk), .R(n_rst), .S(1'b1), .Q(d_edge) );
  XOR2X1 U6 ( .A(d_plus), .B(Q0), .Y(Q1) );
endmodule

