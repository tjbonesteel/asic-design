// $Id: $
// File name:   sensor_s.sv
// Created:     1/25/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: Structural style sensor error detector

module sensor_s
  (
   input wire [3:0] sensors,
   output wire error
		
   );
   
   wire [1:0]  midstep;
   
   and top1(midstep[1], sensors[3], sensors[1]);
   and top2(midstep[0], sensors[2], sensors[1]);
   or bot(error, midstep[1], midstep[0], sensors[0]);

   
endmodule   
   