// $Id: $
// File name:   adder_4bit.sv
// Created:     1/25/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: Connecting 1-bit adder components to make a 4-bit adder

module adder_4bit
  (
   input wire [3:0] a,
   input wire [3:0] b,
   input wire carry_in,
   output wire [3:0] sum,
   output wire overflow

   );

   wire [4:0]  c_flags;

   genvar      i;
   assign c_flags[0] = carry_in;

   generate
      for(i = 0; i <=3; i++)begin
	 adder_1bit adder (.a(a[i]), .b(b[i]), .carry_in(c_flags[i]), .sum(sum[i]), .carry_out(c_flags[i+1]));
      end
      endgenerate
   assign overflow = c_flags[4];
   

endmodule // adder_4bit
