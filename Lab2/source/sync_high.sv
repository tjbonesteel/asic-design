// $Id: $
// File name:   sync_high.sv
// Created:     1/26/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: Reset to logic low synchronizer

module sync_high
  (
   input wire clk,
   input wire n_rst,
   input wire async_in,
   output reg sync_out
   );

   reg 	      Q;

   always_ff @ (posedge clk, negedge n_rst) begin : sync_l
     if(1'b0 == n_rst) begin
	Q <= 1;
	sync_out <= 1;

     end else begin
	Q <= async_in;
	sync_out <= Q;

     end
   end // always_ff @ (posedge clk, negedge n_rst)
endmodule // sync_low
