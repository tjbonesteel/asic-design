// $Id: $
// File name:   sensor_d.sv
// Created:     1/25/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: Dataflow style sensor error detector design

module sensor_d
  (
   input wire [3:0] sensors,
   output wire error
   );
   wire        st1;
   wire        st2;
   
   
   assign st1 = (sensors[3] == 1'b1) ? sensors[1] : 1'b0;
   assign st2 = (sensors[2] == 1'b1) ? sensors[1] : 1'b0;
   assign error = st1 | st2 | sensors[0];
   
endmodule // sensor_d
