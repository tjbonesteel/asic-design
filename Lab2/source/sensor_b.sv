// $Id: $
// File name:   sensor_b.sv
// Created:     1/25/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: Bahavioral style sensor error detector design

module sensor_b
  (
   input wire [3:0] sensors,
   output reg error
   );

   reg [1:0]  midstep;
   
   always_comb
     begin
	error = 1'b0;
	
	if(sensors[3] == 1'b1) midstep[1] = sensors[1];
	else midstep[1] = 1'b0;
	if(sensors[2] == 1'b1) midstep[0] = sensors[1];
	else midstep[0] = 1'b0;
	
	
	if(midstep[1] == 1'b1) error = 1'b1;
	else if(midstep[0] == 1'b1) error = 1'b1;
	else if(sensors[0] == 1'b1) error = 1'b1;
	else error = 1'b0;
	
     end
   
	
   

endmodule // sensor_b
