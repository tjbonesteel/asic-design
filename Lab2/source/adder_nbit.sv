// $Id: $
// File name:   adder_nbit.sv
// Created:     1/25/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: Parameterized Ripple varry adder design
`timescale 1ns / 100 ps
module adder_nbit
  #(
    parameter BIT_WIDTH = 4
    )
   (
    input wire [(BIT_WIDTH - 1):0] a,
    input wire [(BIT_WIDTH - 1):0] b,
    input wire carry_in,
    output wire [(BIT_WIDTH - 1):0] sum,
    output wire carry_out
    );

   wire [(BIT_WIDTH):0] c_flags;

   genvar 		i;
   assign c_flags[0] = carry_in;

   generate
      for(i = 0; i <= (BIT_WIDTH - 1); i = i + 1)begin
	 /*always @(a) begin
	    assert((a[i] == 1'b0) || (a[i] == 1'b1))	
	      else $error("Input 'a' of component is not a digital logic value");
	 end 
	 always @(b) begin
	    assert((b[i] == 1'b0) || (b[i] == 1'b1))	
	      else $error("Input 'b' of component is not a digital logic value");
	 end 
	 always @(carry_in) begin
	    #(2)assert((carry_in == 1'b0) || (carry_in == 1'b1))
	      
	      else $error("Input 'carry_in' of compenet is not a digital logic value");
	 end */
	 adder_1bit adder (.a(a[i]), .b(b[i]), .carry_in(c_flags[i]), .sum(sum[i]), .carry_out(c_flags[i+1]));
	 always @ (a[i],b[i],c_flags[i])
	   begin
	      #(2) assert(((a[i] + b[i] + c_flags[i]) %2) == sum[i])
		else $error("Sum incorrect for 1 bit adder!!");
	   end
	 

      end
   endgenerate
   assign carry_out = c_flags[BIT_WIDTH];

endmodule // adder_nbit

		
