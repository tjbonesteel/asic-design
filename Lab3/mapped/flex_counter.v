/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP1
// Date      : Fri Feb  5 00:39:05 2016
/////////////////////////////////////////////////////////////


module flex_counter ( clk, n_rst, clear, count_enable, rollover_val, count_out, 
        rollover_flag );
  input [3:0] rollover_val;
  output [3:0] count_out;
  input clk, n_rst, clear, count_enable;
  output rollover_flag;
  wire   nr_flag, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46,
         n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60,
         n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74,
         n75, n76, n77, n78, n79, n80;

  DFFSR \c_count_reg[0]  ( .D(n38), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[0]) );
  DFFSR \c_count_reg[1]  ( .D(n37), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[1]) );
  DFFSR \c_count_reg[2]  ( .D(n36), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[2]) );
  DFFSR \c_count_reg[3]  ( .D(n35), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        count_out[3]) );
  DFFSR cr_flag_reg ( .D(nr_flag), .CLK(clk), .R(n_rst), .S(1'b1), .Q(
        rollover_flag) );
  OAI21X1 U37 ( .A(n39), .B(n40), .C(n41), .Y(nr_flag) );
  NAND2X1 U38 ( .A(rollover_flag), .B(n42), .Y(n41) );
  NAND3X1 U39 ( .A(n43), .B(n44), .C(n45), .Y(n40) );
  XOR2X1 U40 ( .A(n46), .B(n47), .Y(n45) );
  XOR2X1 U41 ( .A(rollover_val[2]), .B(count_out[2]), .Y(n47) );
  NAND2X1 U42 ( .A(count_out[1]), .B(count_out[0]), .Y(n46) );
  XOR2X1 U43 ( .A(n48), .B(n49), .Y(n44) );
  XOR2X1 U44 ( .A(n50), .B(n51), .Y(n43) );
  NAND3X1 U45 ( .A(count_enable), .B(n52), .C(n53), .Y(n39) );
  INVX1 U46 ( .A(n54), .Y(n53) );
  OAI21X1 U47 ( .A(n55), .B(n48), .C(n56), .Y(n54) );
  NAND3X1 U48 ( .A(count_out[1]), .B(count_out[0]), .C(count_out[2]), .Y(n48)
         );
  XOR2X1 U49 ( .A(rollover_val[0]), .B(count_out[0]), .Y(n52) );
  OAI22X1 U50 ( .A(n50), .B(n57), .C(n58), .D(n59), .Y(n38) );
  OAI21X1 U51 ( .A(n60), .B(n57), .C(n61), .Y(n37) );
  MUX2X1 U52 ( .B(n62), .A(n63), .S(n64), .Y(n61) );
  NOR2X1 U53 ( .A(clear), .B(n58), .Y(n63) );
  NOR2X1 U54 ( .A(n59), .B(n65), .Y(n62) );
  OAI21X1 U55 ( .A(n66), .B(n57), .C(n67), .Y(n36) );
  MUX2X1 U56 ( .B(n68), .A(n69), .S(n70), .Y(n67) );
  NOR2X1 U57 ( .A(clear), .B(n71), .Y(n69) );
  NOR2X1 U58 ( .A(n59), .B(n72), .Y(n68) );
  OAI22X1 U59 ( .A(n55), .B(n57), .C(n73), .D(n59), .Y(n35) );
  NAND2X1 U60 ( .A(n57), .B(n56), .Y(n59) );
  INVX1 U61 ( .A(clear), .Y(n56) );
  XOR2X1 U62 ( .A(n74), .B(n75), .Y(n73) );
  NOR2X1 U63 ( .A(n76), .B(n55), .Y(n75) );
  NAND2X1 U64 ( .A(n71), .B(n70), .Y(n74) );
  NOR2X1 U65 ( .A(n66), .B(n76), .Y(n70) );
  INVX1 U66 ( .A(n72), .Y(n71) );
  NAND2X1 U67 ( .A(n58), .B(n64), .Y(n72) );
  NOR2X1 U68 ( .A(n60), .B(n76), .Y(n64) );
  INVX1 U69 ( .A(n77), .Y(n76) );
  INVX1 U70 ( .A(count_out[1]), .Y(n60) );
  INVX1 U71 ( .A(n65), .Y(n58) );
  NAND2X1 U72 ( .A(count_out[0]), .B(n77), .Y(n65) );
  NAND3X1 U73 ( .A(n78), .B(n79), .C(n80), .Y(n77) );
  NOR2X1 U74 ( .A(n49), .B(n51), .Y(n80) );
  XOR2X1 U75 ( .A(count_out[1]), .B(rollover_val[1]), .Y(n51) );
  XOR2X1 U76 ( .A(count_out[3]), .B(rollover_val[3]), .Y(n49) );
  XOR2X1 U77 ( .A(rollover_val[0]), .B(n50), .Y(n79) );
  INVX1 U78 ( .A(count_out[0]), .Y(n50) );
  XOR2X1 U79 ( .A(rollover_val[2]), .B(n66), .Y(n78) );
  INVX1 U80 ( .A(count_out[2]), .Y(n66) );
  INVX1 U81 ( .A(n42), .Y(n57) );
  NOR2X1 U82 ( .A(count_enable), .B(clear), .Y(n42) );
  INVX1 U83 ( .A(count_out[3]), .Y(n55) );
endmodule

