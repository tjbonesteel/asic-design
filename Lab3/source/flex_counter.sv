// $Id: $
// File name:   flex_counter.sv
// Created:     2/4/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Flexible and scalable counter with controlled rollover

module flex_counter
   #(
   parameter NUM_CNT_BITS = 4
     )
   (
    input wire clk, n_rst, clear, count_enable,
    input wire [(NUM_CNT_BITS-1):0] rollover_val,
    output reg [(NUM_CNT_BITS-1):0] count_out,
    output reg rollover_flag    
);
			 
   reg [(NUM_CNT_BITS-1):0] c_count;
   reg [(NUM_CNT_BITS-1):0] n_count;
   reg 			    cr_flag = 0;
   reg 			    nr_flag = 0;

   always_ff @(posedge clk, negedge n_rst) begin
      if(n_rst ==0)begin
	 c_count <= '0;
	 cr_flag <= '0;
      end else begin
	 c_count <= n_count;
	 cr_flag <= nr_flag;
      end
   end
   always_comb begin

      nr_flag = cr_flag;

      if(count_enable == 1)begin
	 if ((c_count + 1) == rollover_val)
	    nr_flag = 1'b1;
	 else
	   nr_flag = 1'b0;
      end

      if(clear == 1) begin
	 n_count = 1'b0;
	 nr_flag = 1'b0;
	 
      end else if(count_enable == 1) begin
	 if(c_count == rollover_val)
	   n_count = 1'b1;
	 else
	   n_count = c_count + 1;
      end else
	n_count = c_count;

	 
	 
	 

/*


      
     



      
      always_comb begin
	 if(clear == 1) begin
	    n_count = 1'b0;
	    nr_flag = 1'b0;
	 end else if(count_enable == 1) begin
	    if(n_count == rollover_val)
	      nr_flag = 1'b1;
	    if(n_count == (rollover_val + 1))
	      n_count = 1;
	    
	 end
*/

      end // always_comb begin
 
 
   assign count_out = c_count;
   assign rollover_flag = cr_flag;

endmodule //flex_counter

	 
	      
	    
	 
	 
   