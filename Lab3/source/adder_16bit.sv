// $Id: $
// File name:   sensor_b.sv
// Created:     1/26/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: 16 Bit adder wrapper file
`timescale 1ns / 100 ps
module adder_16bit
(
	input wire [15:0] a,
	input wire [15:0] b,
	input wire carry_in,
	output wire [15:0] sum,
	output wire carry_out
);
   genvar 	    i;

   generate
      for(i = 0; i <= 15; i++) begin
	 always @(a) begin
	    assert((a[i] == 1'b0) || (a[i] == 1'b1))	
	      else $error("Input 'a' of component is not a digital logic value");
	 end 
	 always @(b) begin
	    assert((b[i] == 1'b0) || (b[i] == 1'b1))	
	      else $error("Input 'b' of component is not a digital logic value");
	 end 
	 always @(carry_in) begin
	    assert((carry_in == 1'b0) || (carry_in == 1'b1))	
	      else $error("Input 'carry_in' of compenet is not a digital logic value");
	 end 
      end // for (i = 0; i <= 15; i++)
   endgenerate
   adder_nbit #(16) adder(.a(a), .b(b), .carry_in(carry_in), .sum(sum), .carry_out(carry_out));
   
endmodule
