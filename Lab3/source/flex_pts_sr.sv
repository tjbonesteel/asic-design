// $Id: $
// File name:   flex_pts_sr.sv
// Created:     2/4/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: N-bit Parallel to Serial shift register

module flex_pts_sr
  #(
    parameter NUM_BITS = 4,
    parameter SHIFT_MSB = 1
    )
   (
    input wire clk, n_rst, shift_enable, load_enable,
    input reg [(NUM_BITS-1):0]parallel_in,
    output wire serial_out
    );

   reg [(NUM_BITS-1):0] c_state; //current state
   reg [(NUM_BITS-1):0] n_state; //next state

   always_ff @(posedge clk, negedge n_rst) begin
      if(n_rst == 0)
	c_state <= '1;
      else
	c_state <= n_state;
   end

   always_comb begin
      if(load_enable == 0)begin
	 
	 if(shift_enable == 1) begin
	    if(SHIFT_MSB == 1) 
	       n_state = {c_state[(NUM_BITS-2):0], 1'b1};
	    else
	      n_state = {1'b1, c_state[(NUM_BITS-1):1]};
	 end else
	   n_state = c_state;
	 
      end else
	 n_state = parallel_in;
	 
   end

      if(SHIFT_MSB ==1)
	assign serial_out = c_state[(NUM_BITS-1)];
      else
	assign serial_out = c_state[0];
endmodule // flex_pts_sr
