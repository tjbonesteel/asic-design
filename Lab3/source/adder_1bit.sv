// $Id: $
// File name:   adder_1bit.sv
// Created:     1/25/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: 1-Bit Full adder design
`timescale 1ns / 100 ps
module adder_1bit
  (
   input wire a,
   input wire b,
   input wire carry_in,
   output wire sum,
   output wire carry_out
   );
   
always @ (a, b, carry_in) 
begin
   assert((a == 1'b0) || (a == 1'b1))
     else $error("Input 'a' of component is not a digital logic value!");

   assert((b == 1'b0) || (b == 1'b1))
     else $error("Input 'b' of component is not a digital logic value!");
   
   assert((carry_in == 1'b0) || (carry_in == 1'b1))
     else $error("Input 'carry_in' of component is not a digital logic value!");



end
   
   
  assign sum = carry_in ^ (a ^ b);
  assign carry_out = ((~ carry_in) & b & a) | (carry_in & (b | a));

   always @ (a, b, carry_in) 
     begin
	#(2) assert(((a + b + carry_in) % 2 ) == sum)
	  else $error("Output 'sum' is incoorect!");
     end	
endmodule // adder_1bit
