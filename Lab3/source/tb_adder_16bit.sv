// $Id: $
// File name:   tb_adder_16bit.sv
// Created:     1/26/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: 16 bit adder test bench

`timescale 1ns / 100 ps

module tb_adder_16bit
();

   localparam NUM_INPUT_BITS = 16;
   localparam NUM_OUTPUT_BITS		= NUM_INPUT_BITS + 1;
   localparam MAX_OUTPUT_BIT = NUM_OUTPUT_BITS - 1;
   localparam NUM_TEST_BITS = (NUM_INPUT_BITS * 2) + 1;
   localparam MAX_TEST_BIT = NUM_TEST_BITS -1;
   localparam NUM_TEST_CASES = 2 ** NUM_TEST_BITS;
   localparam MAX_TEST_VALUE = NUM_TEST_CASES - 1;
   localparam TEST_A_BIT = 0;
   localparam TEST_B_BIT = NUM_INPUT_BITS;
   localparam TEST_CARRY_IN_BIT = MAX_TEST_BIT;
   localparam TEST_SUM_BIT = 0;
   localparam TEST_CARRY_OUT_BIT = MAX_OUTPUT_BIT;
   localparam TEST_DELAY = 10;
   

   wire [15:0]tb_a;
   wire [15:0] tb_b;
   wire        tb_carry_in;
   wire [15:0] tb_sum;
   wire        tb_carry_out;
   wire        no_match;
   

   reg [(NUM_INPUT_BITS - 1):0] tmp_a;
   reg [(NUM_INPUT_BITS - 1):0] tmp_b;
   reg tmp_carry_in;
   reg tmp_no_match = 1;
   reg [MAX_OUTPUT_BIT:0] 	tb_expected_output;
   
   
   
   //DUT port map
   adder_16bit DUT(.a(tb_a), .b(tb_b), .carry_in(tb_carry_in), .sum(tb_sum), .carry_out(tb_carry_out));
   
   assign tb_a = tmp_a;
   assign tb_b = tmp_b;
   assign tb_carry_in = tmp_carry_in;
   assign no_match = tmp_no_match;
   
		   
		   
   initial
     ////////TEST 1////////
  
     begin
	tb_expected_output = 16'b0;
	
	tmp_a = 16'h0000;
	tmp_b = 16'h0000;
	tmp_carry_in = 16'b0;
	#1;
	
	tb_expected_output = tb_a + tb_b + tb_carry_in;
	#(TEST_DELAY - 1);

	 assert(tb_expected_output == tb_sum)
	  begin
	     $info("Correct Sum value for test case 1!");
	     tmp_no_match = 0;
	  end else begin
	     $error("Incorrect sum value for test case 1!");
	     tmp_no_match = 1;
	  end
	assert(tb_expected_output[TEST_CARRY_OUT_BIT] == tb_carry_out)
	  begin
	     $info("Correct carry value for test case 1!");
	  end else begin
	     $error("Incorrect carry value for test case 1!");
	  end
	
	
   
   

   
  
	///////TEST 2////////
	tmp_a = 16'hFEFE;
	tmp_b = 16'h0005;
	
	tmp_carry_in = 16'b1;
	#1;
	
	tb_expected_output = tb_a + tb_b + tb_carry_in;
	#(TEST_DELAY - 1);
	
	assert(tb_expected_output == tb_sum)
	  begin
	     $info("Correct Sum value for test case 2!");
	     tmp_no_match = 0;
	  end else begin
	     $error("Incorrect sum value for test case 2!");
	     tmp_no_match = 1;
	  end
	assert(tb_expected_output[TEST_CARRY_OUT_BIT] == tb_carry_out)
	  begin
	     $info("Correct carry value for test case 2!");
	  end else begin
	     $error("Incorrect carry value for test case 2!");
	  end
  


   
  
	///////TEST 3////////
	tmp_a = 16'h0005;
	tmp_b = 16'h7FAE;
	
	tmp_carry_in = 16'b0;
	#1;
	
	tb_expected_output = tb_a + tb_b + tb_carry_in;
	#(TEST_DELAY - 1);
      
	assert(tb_expected_output == tb_sum)
	  begin
	     $info("Correct Sum value for test case 3!");
	     tmp_no_match = 0;
	  end else begin
	     $error("Incorrect sum value for test case 3!");
	     tmp_no_match = 1;
	 end
	assert(tb_expected_output[TEST_CARRY_OUT_BIT] == tb_carry_out)
	  begin
	     $info("Correct carry value for test case 3!");
	  end else begin
	     $error("Incorrect carry value for test case 3!");
	  end


	///////TEST 4////////
	tmp_a = 16'h0AFA;
	tmp_b = 16'h297F;
	
	tmp_carry_in = 16'b1;
	#1;
	
	tb_expected_output = tb_a + tb_b + tb_carry_in;
	#(TEST_DELAY - 1);
	
	assert(tb_expected_output == tb_sum)
	  begin
	     $info("Correct Sum value for test case 4!");
	     tmp_no_match = 0;
	  end else begin
	     $error("Incorrect sum value for test case 4!");
	     tmp_no_match = 1;
	  end
	assert(tb_expected_output[TEST_CARRY_OUT_BIT] == tb_carry_out)
	  begin
	     $info("Correct carry value for test case 4!");
	  end else begin
	     $error("Incorrect carry value for test case 4!");
	  end
	
	
	///////TEST 5////////
	tmp_a = 16'h0010;
	tmp_b = 16'h000A;
	
	tmp_carry_in = 16'b0;
	#1;
	
	tb_expected_output = tb_a + tb_b + tb_carry_in;
	#(TEST_DELAY - 1);

	assert(tb_expected_output == tb_sum)
	  begin
	     $info("Correct Sum value for test case 5!");
	     tmp_no_match = 0;
	  end else begin
	     $error("Incorrect sum value for test case 5!");
	     tmp_no_match = 1;
	 end
	assert(tb_expected_output[TEST_CARRY_OUT_BIT] == tb_carry_out)
	  begin
	     $info("Correct carry value for test case 5!");
	  end else begin
	     $error("Incorrect carry value for test case 5!");
	  end
      
   
   	///////TEST 6////////
	tmp_a = 16'h0B10;
	tmp_b = 16'h5008;
	
	tmp_carry_in = 16'b0;
	#1;
	
	tb_expected_output = tb_a + tb_b + tb_carry_in;
	#(TEST_DELAY - 1);

	assert(tb_expected_output == tb_sum)
	  begin
	     $info("Correct Sum value for test case 5!");
	     tmp_no_match = 0;
	  end else begin
	     $error("Incorrect sum value for test case 5!");
	     tmp_no_match = 1;
	 end
	assert(tb_expected_output[TEST_CARRY_OUT_BIT] == tb_carry_out)
	  begin
	     $info("Correct carry value for test case 5!");
	  end else begin
	     $error("Incorrect carry value for test case 5!");
	  end
    


   	///////TEST 7////////
	tmp_a = 16'h010B;
	tmp_b = 16'hE580;
	
	tmp_carry_in = 16'b1;
	#1;
	
	tb_expected_output = tb_a + tb_b + tb_carry_in;
	#(TEST_DELAY - 1);

	assert(tb_expected_output == tb_sum)
	  begin
	     $info("Correct Sum value for test case 5!");
	     tmp_no_match = 0;
	  end else begin
	     $error("Incorrect sum value for test case 5!");
	     tmp_no_match = 1;
	 end
	assert(tb_expected_output[TEST_CARRY_OUT_BIT] == tb_carry_out)
	  begin
	     $info("Correct carry value for test case 5!");
	  end else begin
	     $error("Incorrect carry value for test case 5!");
	  end // UNMATCHED !!
	
	////////TEST 8////////
	
	tmp_a = 16'h0001;
	tmp_b = 16'h7fff;
	tmp_carry_in = 16'b0;
	#1;
	
	tb_expected_output = tb_a + tb_b + tb_carry_in;
	#(TEST_DELAY - 1);

	 assert(tb_expected_output == tb_sum)
	  begin
	     $info("Correct Sum value for test case 1!");
	     tmp_no_match = 0;
	  end else begin
	     $error("Incorrect sum value for test case 1!");
	     tmp_no_match = 1;
	  end
	assert(tb_expected_output[TEST_CARRY_OUT_BIT] == tb_carry_out)
	  begin
	     $info("Correct carry value for test case 1!");
	  end else begin
	     $error("Incorrect carry value for test case 1!");
	  end // UNMATCHED !!
	
	////////TEST 8////////
	
	tmp_a = 16'h7ffe;
	tmp_b = 16'h0001;
	tmp_carry_in = 16'b1;
	#1;
	
	tb_expected_output = tb_a + tb_b + tb_carry_in;
	#(TEST_DELAY - 1);

	 assert(tb_expected_output == tb_sum)
	  begin
	     $info("Correct Sum value for test case 1!");
	     tmp_no_match = 0;
	  end else begin
	     $error("Incorrect sum value for test case 1!");
	     tmp_no_match = 1;
	  end
	assert(tb_expected_output[TEST_CARRY_OUT_BIT] == tb_carry_out)
	  begin
	     $info("Correct carry value for test case 1!");
	  end else begin
	     $error("Incorrect carry value for test case 1!");
	  end // UNMATCHED !!
	
	tmp_a = 16'h0000;
	tmp_b = 16'h0000;
	tmp_carry_in = 16'b0;
	#1;
	
	tb_expected_output = tb_a + tb_b + tb_carry_in + 1;
	#(TEST_DELAY - 1);

	 assert(tb_expected_output == tb_sum)
	  begin
	     $info("TESTING_N0_MATCH ---- PASSED");
	     tmp_no_match = 0;
	  end else begin
	     $error("Incorrect sum value for test case 1!");
	     tmp_no_match = 1;
	  end
	assert(tb_expected_output[TEST_CARRY_OUT_BIT] == tb_carry_out)
	  begin
	     $info("TESTING_NO_MATCH! ---- PASSED");
	  end else begin
	     $error("Incorrect carry value for test case 1!");
	  end

	

	
     end // initial begin
   
 
	
	
     
   
endmodule // tb_adder_16bit
