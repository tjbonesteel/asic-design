# This file specifies how the pads are placed
# The name of each pad here has to match the
# name in the verilog code
# The Mosis padframe has 4 corners and 40 pads

Version: 2


Pad: U3 E
Pad: U4 E
Pad: U5 E
Pad: U6 E
Pad: U8 E
Pad: U9 E
Pad: U10 N
Pad: U11 N
Pad: U12 N

Pad: U1 S
Pad: U2 S

Pad: U13 N
Pad: U14 N
Pad: U15 N
Pad: U16 N
Pad: U17 N
Pad: U18 N

Pad: U7 S

Pad: U19 E PADNC
Pad: U20 E PADNC
Pad: U21 E PADNC 
Pad: U22 E PADNC
Pad: U23 N PADNC
Pad: U24 S PADNC
Pad: U25 S PADNC
Pad: U26 S PADNC
Pad: U27 S PADNC
Pad: U28 S PADNC
Pad: U29 S PADNC
Pad: U30 S PADNC
Pad: U31 W PADNC
Pad: U32 W PADNC
Pad: U33 W PADNC
Pad: U34 W PADNC
Pad: U35 W PADNC
Pad: U36 W PADNC
Pad: U37 W PADNC
Pad: U38 W PADNC
Pad: U39 W PADNC
Pad: U40 W PADNC

Orient: R0
Pad: c01 NW  PADFC

Orient: R270
Pad: c02 NE PADFC

Orient: R180
Pad: c03 SE PADFC

Orient: R90
Pad: c04 SW PADFC
